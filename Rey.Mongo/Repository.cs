﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using Rey.Mongo.Attributes;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace Rey.Mongo {
    public class Repository<T> : IRepository<T> {
        public IMongoCollection<T> Collection { get; }
        public CollectionNamespace CollectionNamespace => this.Collection.CollectionNamespace;
        public IMongoDatabase Database => this.Collection.Database;
        public IBsonSerializer<T> DocumentSerializer => this.Collection.DocumentSerializer;
        public IMongoIndexManager<T> Indexes => this.Collection.Indexes;
        public MongoCollectionSettings Settings => this.Collection.Settings;

        public Repository(IMongoDatabase database) {
            var type = typeof(T);
            var name = type.GetCustomAttribute<CollectionAttribute>()?.Name ?? type.Name;
            this.Collection = database.GetCollection<T>(name);
        }

        public IAsyncCursor<TResult> Aggregate<TResult>(
            PipelineDefinition<T, TResult> pipeline,
            AggregateOptions options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.Aggregate(pipeline, options, cancellationToken);

        public IAsyncCursor<TResult> Aggregate<TResult>(
            IClientSessionHandle session,
            PipelineDefinition<T, TResult> pipeline,
            AggregateOptions options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.Aggregate(session, pipeline, options, cancellationToken);

        public Task<IAsyncCursor<TResult>> AggregateAsync<TResult>(
            PipelineDefinition<T, TResult> pipeline,
            AggregateOptions options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.AggregateAsync(pipeline, options, cancellationToken);

        public Task<IAsyncCursor<TResult>> AggregateAsync<TResult>(
            IClientSessionHandle session,
            PipelineDefinition<T, TResult> pipeline,
            AggregateOptions options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.AggregateAsync(session, pipeline, options, cancellationToken);

        public BulkWriteResult<T> BulkWrite(
            IEnumerable<WriteModel<T>> requests,
            BulkWriteOptions options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.BulkWrite(requests, options, cancellationToken);

        public BulkWriteResult<T> BulkWrite(
            IClientSessionHandle session,
            IEnumerable<WriteModel<T>> requests,
            BulkWriteOptions options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.BulkWrite(session, requests, options, cancellationToken);

        public Task<BulkWriteResult<T>> BulkWriteAsync(
            IEnumerable<WriteModel<T>> requests,
            BulkWriteOptions options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.BulkWriteAsync(requests, options, cancellationToken);

        public Task<BulkWriteResult<T>> BulkWriteAsync(
            IClientSessionHandle session,
            IEnumerable<WriteModel<T>> requests,
            BulkWriteOptions options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.BulkWriteAsync(session, requests, options, cancellationToken);

        public long Count(
            FilterDefinition<T> filter,
            CountOptions options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.Count(filter, options, cancellationToken);

        public long Count(
            IClientSessionHandle session,
            FilterDefinition<T> filter,
            CountOptions options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.Count(session, filter, options, cancellationToken);

        public Task<long> CountAsync(
            FilterDefinition<T> filter,
            CountOptions options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.CountAsync(filter, options, cancellationToken);

        public Task<long> CountAsync(
            IClientSessionHandle session,
            FilterDefinition<T> filter,
            CountOptions options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.CountAsync(session, filter, options, cancellationToken);

        public long CountDocuments(
            FilterDefinition<T> filter,
            CountOptions options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.CountDocuments(filter, options, cancellationToken);

        public long CountDocuments(
            IClientSessionHandle session,
            FilterDefinition<T> filter,
            CountOptions options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.CountDocuments(session, filter, options, cancellationToken);

        public Task<long> CountDocumentsAsync(
            FilterDefinition<T> filter,
            CountOptions options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.CountDocumentsAsync(filter, options, cancellationToken);

        public Task<long> CountDocumentsAsync(
            IClientSessionHandle session,
            FilterDefinition<T> filter,
            CountOptions options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.CountDocumentsAsync(session, filter, options, cancellationToken);

        public DeleteResult DeleteMany(
            FilterDefinition<T> filter,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.DeleteMany(filter, cancellationToken);

        public DeleteResult DeleteMany(
            FilterDefinition<T> filter,
            DeleteOptions options,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.DeleteMany(filter, options, cancellationToken);

        public DeleteResult DeleteMany(
            IClientSessionHandle session,
            FilterDefinition<T> filter,
            DeleteOptions options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.DeleteMany(session, filter, options, cancellationToken);

        public Task<DeleteResult> DeleteManyAsync(
            FilterDefinition<T> filter,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.DeleteManyAsync(filter, cancellationToken);

        public Task<DeleteResult> DeleteManyAsync(
            FilterDefinition<T> filter,
            DeleteOptions options,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.DeleteManyAsync(filter, options, cancellationToken);

        public Task<DeleteResult> DeleteManyAsync(
            IClientSessionHandle session,
            FilterDefinition<T> filter,
            DeleteOptions options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.DeleteManyAsync(session, filter, options, cancellationToken);

        public DeleteResult DeleteOne(
            FilterDefinition<T> filter,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.DeleteOne(filter, cancellationToken);

        public DeleteResult DeleteOne(
            FilterDefinition<T> filter,
            DeleteOptions options,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.DeleteOne(filter, options, cancellationToken);

        public DeleteResult DeleteOne(
            IClientSessionHandle session,
            FilterDefinition<T> filter,
            DeleteOptions options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.DeleteOne(session, filter, options, cancellationToken);

        public Task<DeleteResult> DeleteOneAsync(
            FilterDefinition<T> filter,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.DeleteOneAsync(filter, cancellationToken);

        public Task<DeleteResult> DeleteOneAsync(
            FilterDefinition<T> filter,
            DeleteOptions options,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.DeleteOneAsync(filter, options, cancellationToken);

        public Task<DeleteResult> DeleteOneAsync(
            IClientSessionHandle session,
            FilterDefinition<T> filter,
            DeleteOptions options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.DeleteOneAsync(session, filter, options, cancellationToken);

        public IAsyncCursor<TField> Distinct<TField>(
            FieldDefinition<T, TField> field,
            FilterDefinition<T> filter,
            DistinctOptions options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.Distinct(field, filter, options, cancellationToken);

        public IAsyncCursor<TField> Distinct<TField>(
            IClientSessionHandle session,
            FieldDefinition<T, TField> field,
            FilterDefinition<T> filter,
            DistinctOptions options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.Distinct(session, field, filter, options, cancellationToken);

        public Task<IAsyncCursor<TField>> DistinctAsync<TField>(
            FieldDefinition<T, TField> field,
            FilterDefinition<T> filter,
            DistinctOptions options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.DistinctAsync(field, filter, options, cancellationToken);

        public Task<IAsyncCursor<TField>> DistinctAsync<TField>(
            IClientSessionHandle session,
            FieldDefinition<T, TField> field,
            FilterDefinition<T> filter,
            DistinctOptions options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.DistinctAsync(session, field, filter, options, cancellationToken);

        public long EstimatedDocumentCount(
            EstimatedDocumentCountOptions options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.EstimatedDocumentCount(options, cancellationToken);

        public Task<long> EstimatedDocumentCountAsync(
            EstimatedDocumentCountOptions options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.EstimatedDocumentCountAsync(options, cancellationToken);

        public Task<IAsyncCursor<TProjection>> FindAsync<TProjection>(
            FilterDefinition<T> filter,
            FindOptions<T, TProjection> options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.FindAsync(filter, options, cancellationToken);

        public Task<IAsyncCursor<TProjection>> FindAsync<TProjection>(
            IClientSessionHandle session,
            FilterDefinition<T> filter,
            FindOptions<T, TProjection> options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.FindAsync(session, filter, options, cancellationToken);

        public TProjection FindOneAndDelete<TProjection>(
            FilterDefinition<T> filter,
            FindOneAndDeleteOptions<T, TProjection> options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.FindOneAndDelete(filter, options, cancellationToken);

        public TProjection FindOneAndDelete<TProjection>(
            IClientSessionHandle session,
            FilterDefinition<T> filter,
            FindOneAndDeleteOptions<T, TProjection> options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.FindOneAndDelete(session, filter, options, cancellationToken);

        public Task<TProjection> FindOneAndDeleteAsync<TProjection>(
            FilterDefinition<T> filter,
            FindOneAndDeleteOptions<T, TProjection> options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.FindOneAndDeleteAsync(filter, options, cancellationToken);

        public Task<TProjection> FindOneAndDeleteAsync<TProjection>(
            IClientSessionHandle session,
            FilterDefinition<T> filter,
            FindOneAndDeleteOptions<T, TProjection> options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.FindOneAndDeleteAsync(session, filter, options, cancellationToken);

        public TProjection FindOneAndReplace<TProjection>(
            FilterDefinition<T> filter,
            T replacement,
            FindOneAndReplaceOptions<T, TProjection> options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.FindOneAndReplace(filter, replacement, options, cancellationToken);

        public TProjection FindOneAndReplace<TProjection>(
            IClientSessionHandle session,
            FilterDefinition<T> filter,
            T replacement,
            FindOneAndReplaceOptions<T, TProjection> options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.FindOneAndReplace(session, filter, replacement, options, cancellationToken);

        public Task<TProjection> FindOneAndReplaceAsync<TProjection>(
            FilterDefinition<T> filter,
            T replacement,
            FindOneAndReplaceOptions<T, TProjection> options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.FindOneAndReplaceAsync(filter, replacement, options, cancellationToken);

        public Task<TProjection> FindOneAndReplaceAsync<TProjection>(
            IClientSessionHandle session,
            FilterDefinition<T> filter,
            T replacement,
            FindOneAndReplaceOptions<T, TProjection> options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.FindOneAndReplaceAsync(session, filter, replacement, options, cancellationToken);

        public TProjection FindOneAndUpdate<TProjection>(
            FilterDefinition<T> filter,
            UpdateDefinition<T> update,
            FindOneAndUpdateOptions<T, TProjection> options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.FindOneAndUpdate(filter, update, options, cancellationToken);

        public TProjection FindOneAndUpdate<TProjection>(
            IClientSessionHandle session,
            FilterDefinition<T> filter,
            UpdateDefinition<T> update,
            FindOneAndUpdateOptions<T, TProjection> options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.FindOneAndUpdate(session, filter, update, options, cancellationToken);

        public Task<TProjection> FindOneAndUpdateAsync<TProjection>(
            FilterDefinition<T> filter,
            UpdateDefinition<T> update,
            FindOneAndUpdateOptions<T, TProjection> options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.FindOneAndUpdateAsync(filter, update, options, cancellationToken);

        public Task<TProjection> FindOneAndUpdateAsync<TProjection>(
            IClientSessionHandle session,
            FilterDefinition<T> filter,
            UpdateDefinition<T> update,
            FindOneAndUpdateOptions<T, TProjection> options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.FindOneAndUpdateAsync(session, filter, update, options, cancellationToken);

        public IAsyncCursor<TProjection> FindSync<TProjection>(
            FilterDefinition<T> filter,
            FindOptions<T, TProjection> options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.FindSync(filter, options, cancellationToken);

        public IAsyncCursor<TProjection> FindSync<TProjection>(
            IClientSessionHandle session,
            FilterDefinition<T> filter,
            FindOptions<T, TProjection> options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.FindSync(session, filter, options, cancellationToken);

        public void InsertMany(
            IEnumerable<T> documents,
            InsertManyOptions options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.InsertMany(documents, options, cancellationToken);

        public void InsertMany(
            IClientSessionHandle session,
            IEnumerable<T> documents,
            InsertManyOptions options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.InsertMany(session, documents, options, cancellationToken);

        public Task InsertManyAsync(
            IEnumerable<T> documents,
            InsertManyOptions options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.InsertManyAsync(documents, options, cancellationToken);

        public Task InsertManyAsync(
            IClientSessionHandle session,
            IEnumerable<T> documents,
            InsertManyOptions options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.InsertManyAsync(session, documents, options, cancellationToken);

        public void InsertOne(
            T document,
            InsertOneOptions options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.InsertOne(document, options, cancellationToken);

        public void InsertOne(
            IClientSessionHandle session,
            T document,
            InsertOneOptions options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.InsertOne(session, document, options, cancellationToken);

        public Task InsertOneAsync(
            T document,
            CancellationToken cancellationToken)
            => this.Collection.InsertOneAsync(document, cancellationToken);

        public Task InsertOneAsync(
            T document,
            InsertOneOptions options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.InsertOneAsync(document, options, cancellationToken);

        public Task InsertOneAsync(
            IClientSessionHandle session,
            T document,
            InsertOneOptions options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.InsertOneAsync(session, document, options, cancellationToken);

        public IAsyncCursor<TResult> MapReduce<TResult>(
            BsonJavaScript map,
            BsonJavaScript reduce,
            MapReduceOptions<T, TResult> options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.MapReduce(map, reduce, options, cancellationToken);

        public IAsyncCursor<TResult> MapReduce<TResult>(
            IClientSessionHandle session,
            BsonJavaScript map,
            BsonJavaScript reduce,
            MapReduceOptions<T, TResult> options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.MapReduce(session, map, reduce, options, cancellationToken);

        public Task<IAsyncCursor<TResult>> MapReduceAsync<TResult>(
            BsonJavaScript map,
            BsonJavaScript reduce,
            MapReduceOptions<T, TResult> options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.MapReduceAsync(map, reduce, options, cancellationToken);

        public Task<IAsyncCursor<TResult>> MapReduceAsync<TResult>(
            IClientSessionHandle session,
            BsonJavaScript map,
            BsonJavaScript reduce,
            MapReduceOptions<T, TResult> options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.MapReduceAsync(session, map, reduce, options, cancellationToken);

        public IFilteredMongoCollection<TDerivedDocument> OfType<TDerivedDocument>()
            where TDerivedDocument : T
            => this.Collection.OfType<TDerivedDocument>();

        public ReplaceOneResult ReplaceOne(
            FilterDefinition<T> filter,
            T replacement,
            UpdateOptions options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.ReplaceOne(filter, replacement, options, cancellationToken);

        public ReplaceOneResult ReplaceOne(
            IClientSessionHandle session,
            FilterDefinition<T> filter,
            T replacement,
            UpdateOptions options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.ReplaceOne(session, filter, replacement, options, cancellationToken);

        public Task<ReplaceOneResult> ReplaceOneAsync(
            FilterDefinition<T> filter,
            T replacement,
            UpdateOptions options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.ReplaceOneAsync(filter, replacement, options, cancellationToken);

        public Task<ReplaceOneResult> ReplaceOneAsync(
            IClientSessionHandle session,
            FilterDefinition<T> filter,
            T replacement,
            UpdateOptions options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.ReplaceOneAsync(session, filter, replacement, options, cancellationToken);

        public UpdateResult UpdateMany(
            FilterDefinition<T> filter,
            UpdateDefinition<T> update,
            UpdateOptions options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.UpdateMany(filter, update, options, cancellationToken);

        public UpdateResult UpdateMany(
            IClientSessionHandle session,
            FilterDefinition<T> filter,
            UpdateDefinition<T> update,
            UpdateOptions options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.UpdateMany(session, filter, update, options, cancellationToken);

        public Task<UpdateResult> UpdateManyAsync(
            FilterDefinition<T> filter,
            UpdateDefinition<T> update,
            UpdateOptions options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.UpdateManyAsync(filter, update, options, cancellationToken);

        public Task<UpdateResult> UpdateManyAsync(
            IClientSessionHandle session,
            FilterDefinition<T> filter,
            UpdateDefinition<T> update,
            UpdateOptions options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.UpdateManyAsync(session, filter, update, options, cancellationToken);

        public UpdateResult UpdateOne(
            FilterDefinition<T> filter,
            UpdateDefinition<T> update,
            UpdateOptions options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.UpdateOne(filter, update, options, cancellationToken);

        public UpdateResult UpdateOne(
            IClientSessionHandle session,
            FilterDefinition<T> filter,
            UpdateDefinition<T> update,
            UpdateOptions options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.UpdateOne(session, filter, update, options, cancellationToken);

        public Task<UpdateResult> UpdateOneAsync(
            FilterDefinition<T> filter,
            UpdateDefinition<T> update,
            UpdateOptions options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.UpdateOneAsync(filter, update, options, cancellationToken);

        public Task<UpdateResult> UpdateOneAsync(
            IClientSessionHandle session,
            FilterDefinition<T> filter,
            UpdateDefinition<T> update,
            UpdateOptions options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.UpdateOneAsync(session, filter, update, options, cancellationToken);

        public IAsyncCursor<TResult> Watch<TResult>(
            PipelineDefinition<ChangeStreamDocument<T>, TResult> pipeline,
            ChangeStreamOptions options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.Watch(pipeline, options, cancellationToken);

        public IAsyncCursor<TResult> Watch<TResult>(
            IClientSessionHandle session,
            PipelineDefinition<ChangeStreamDocument<T>, TResult> pipeline,
            ChangeStreamOptions options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.Watch(session, pipeline, options, cancellationToken);

        public Task<IAsyncCursor<TResult>> WatchAsync<TResult>(
            PipelineDefinition<ChangeStreamDocument<T>, TResult> pipeline,
            ChangeStreamOptions options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.WatchAsync(pipeline, options, cancellationToken);

        public Task<IAsyncCursor<TResult>> WatchAsync<TResult>(
            IClientSessionHandle session,
            PipelineDefinition<ChangeStreamDocument<T>, TResult> pipeline,
            ChangeStreamOptions options = null,
            CancellationToken cancellationToken = default(CancellationToken))
            => this.Collection.WatchAsync(session, pipeline, options, cancellationToken);

        public IMongoCollection<T> WithReadConcern(
            ReadConcern readConcern
            )
            => this.Collection.WithReadConcern(readConcern);

        public IMongoCollection<T> WithReadPreference(
            ReadPreference readPreference
            )
            => this.Collection.WithReadPreference(readPreference);

        public IMongoCollection<T> WithWriteConcern(
            WriteConcern writeConcern
            )
            => this.Collection.WithWriteConcern(writeConcern);
    }
}
