﻿using System;

namespace Rey.Mongo.Attributes {
    [AttributeUsage(AttributeTargets.Class)]
    public class CollectionAttribute : Attribute {
        public string Name { get; set; }
        public CollectionAttribute(string name = null) {
            this.Name = name;
        }
    }

    [AttributeUsage(AttributeTargets.Class)]
    public class RefModelAttribute : Attribute {
        public Type Type { get; set; }
        public RefModelAttribute(Type type) {
            this.Type = type;
        }
    }
}
