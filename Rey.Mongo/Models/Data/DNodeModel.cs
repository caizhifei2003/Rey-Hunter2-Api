﻿namespace Rey.Mongo.Models.Data {
    public abstract class DNodeModel : DModel {
        public string ParentId { get; set; }
        public string Name { get; set; }
    }
}
