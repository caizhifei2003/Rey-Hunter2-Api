﻿using System.Collections.Generic;

namespace Rey.Mongo.Models.View {
    public class VMPageData<TModel> {
        public IEnumerable<TModel> Items { get; set; }
        public int Page { get; set; }
        public int Size { get; set; }
        public long Total { get; set; }
    }
}
