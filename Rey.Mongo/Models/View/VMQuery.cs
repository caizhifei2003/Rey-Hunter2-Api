﻿namespace Rey.Mongo.Models.View {
    public abstract class VMQuery {
        public string Search { get; set; }
        public string Sort { get; set; } = "desc";
        public string SortBy { get; set; } = "CreateAt";
    }
}
