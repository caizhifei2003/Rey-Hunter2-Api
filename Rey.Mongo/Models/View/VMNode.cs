﻿using System.Collections.Generic;

namespace Rey.Mongo.Models.View {
    public abstract class VMNode<TNode> : VMNodeModel
        where TNode : VMNode<TNode> {
        public List<TNode> Children { get; set; } = new List<TNode>();
    }
}
