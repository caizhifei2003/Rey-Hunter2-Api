﻿using System;

namespace Rey.Mongo.Models.View {
    public abstract class VMModel {
        public string Id { get; set; }
        public DateTime? CreateAt { get; set; } = DateTime.Now;
    }
}
