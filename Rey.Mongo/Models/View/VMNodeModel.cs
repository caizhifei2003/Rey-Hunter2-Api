﻿namespace Rey.Mongo.Models.View {
    public abstract class VMNodeModel : VMModel {
        public string ParentId { get; set; }
        public string Name { get; set; }
    }
}
