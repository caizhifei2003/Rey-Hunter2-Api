﻿using Rey.Mongo.Models.Data;
using Rey.Mongo.Models.View;

namespace Rey.Mongo.Models {
    public abstract class RefModel {
        public string Id { get; set; }
    }

    public abstract class RefModel<TDModel> : RefModel
        where TDModel : DModel {

    }

    public abstract class RefModel<TDModel, TVMModel> : RefModel<TDModel>
        where TDModel : DModel
        where TVMModel : VMModel {

    }
}
