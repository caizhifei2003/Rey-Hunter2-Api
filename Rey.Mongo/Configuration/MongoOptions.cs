﻿namespace Rey.Mongo.Configuration {
    public class MongoOptions {
        public string Host { get; set; } = "localhost";
        public int Port { get; set; } = 27017;
        public string User { get; set; }
        public string Password { get; set; }
        public string Database { get; set; }
    }
}
