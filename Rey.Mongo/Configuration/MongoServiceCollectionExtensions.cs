﻿using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using Rey.Mongo;
using Rey.Mongo.Configuration;
using System;

namespace Microsoft.Extensions.DependencyInjection {
    public static class MongoServiceCollectionExtensions {
        public static IServiceCollection AddMongo(this IServiceCollection services, Action<MongoOptions> configure) {
            var options = new MongoOptions();
            configure?.Invoke(options);
            return AddMongo(services, options);
        }

        public static IServiceCollection AddMongo(this IServiceCollection services, MongoOptions options) {
            //! 全局格式化UTC时间为本地时间
            BsonSerializer.RegisterSerializer(DateTimeSerializer.LocalInstance);

            services.AddSingleton(options);

            services.AddSingleton<IMongoClient>(provider => new MongoClient(GetSettings(provider.GetService<MongoOptions>())));
            services.AddSingleton<IMongoDatabase>(provider => provider.GetService<IMongoClient>().GetDatabase(provider.GetService<MongoOptions>().Database));
            services.AddSingleton(typeof(IRepository<>), typeof(Repository<>));
            services.AddSingleton(typeof(IMongoCollection<>), typeof(Repository<>));
            return services;
        }

        private static MongoClientSettings GetSettings(MongoOptions options) {
            var settings = new MongoClientSettings();
            settings.Server = new MongoServerAddress(options.Host, options.Port);
            if (options.User != null) {
                settings.Credential = MongoCredential.CreateCredential("admin", options.User, options.Password);
            }
            return settings;
        }
    }
}
