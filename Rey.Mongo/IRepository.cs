﻿using MongoDB.Driver;

namespace Rey.Mongo {
    public interface IRepository<T> : IMongoCollection<T> {
        IMongoCollection<T> Collection { get; }

    }
}
