﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace Rey.Security.Jwt {
    public class JwtTokenGenerator : IJwtTokenGenerator {
        private IJwtOptions Options { get; }

        public JwtTokenGenerator(IJwtOptions options) {
            this.Options = options;
        }

        public IJwtToken Generate(IEnumerable<Claim> claims, double? expiresIn = null) {
            var now = DateTime.UtcNow;
            var seconds = expiresIn ?? this.Options.ExpiresIn;
            var expires = now.AddSeconds(seconds);

            var token = new JwtSecurityToken(
                issuer: this.Options.Issuer,
                audience: this.Options.Audience,
                claims: claims,
                expires: expires,
                signingCredentials: new SigningCredentials(this.Options.GetSecureKey(), SecurityAlgorithms.HmacSha256));

            return new JwtToken(token, seconds, now);
        }
    }
}
