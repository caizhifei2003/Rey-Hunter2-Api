﻿using System;

namespace Rey.Security.Jwt {
    public interface IJwtToken {
        double ExpiresIn { get; }
        DateTime Date { get; }
        long Timestamp { get; }
        string ToString();
    }
}
