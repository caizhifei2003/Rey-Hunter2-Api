﻿using Microsoft.IdentityModel.Tokens;

namespace Rey.Security.Jwt {
    public interface IJwtOptions {
        string Issuer { get; }
        string Audience { get; }
        double ExpiresIn { get; }
        string Key { get; }
        SecurityKey GetSecureKey();
    }
}
