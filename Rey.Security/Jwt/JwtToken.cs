﻿using System;
using System.IdentityModel.Tokens.Jwt;

namespace Rey.Security.Jwt {
    public class JwtToken : IJwtToken {
        private static readonly long UnixEpochTicks = (new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).Ticks;

        private JwtSecurityToken Token { get; }
        public double ExpiresIn { get; }
        public DateTime Date { get; }
        public long Timestamp => (this.Date.ToUniversalTime().Ticks - UnixEpochTicks) / 10000;

        public JwtToken(JwtSecurityToken token, double expiresIn, DateTime date) {
            this.Token = token ?? throw new ArgumentNullException(nameof(token));
            this.ExpiresIn = expiresIn;
            this.Date = date;
        }

        public override string ToString() {
            return new JwtSecurityTokenHandler().WriteToken(this.Token);
        }
    }
}
