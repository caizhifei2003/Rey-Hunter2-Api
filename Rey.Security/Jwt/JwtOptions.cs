﻿using Microsoft.IdentityModel.Tokens;
using System.Security.Cryptography;
using System.Text;

namespace Rey.Security.Jwt {
    public class JwtOptions : IJwtOptions {
        public string Issuer { get; set; } = "issuer";
        public string Audience { get; set; } = "audience";
        public double ExpiresIn { get; set; } = 7200;
        public string Key { get; set; } = "com.reythink.jwt.key";
        public JwtValidation Validation { get; set; } = new JwtValidation();

        public SecurityKey GetSecureKey() {
            return new SymmetricSecurityKey(SHA1.Create().ComputeHash(Encoding.UTF8.GetBytes(this.Key)));
        }

        public TokenValidationParameters GetValidationParameters() {
            return new TokenValidationParameters {
                ValidateIssuer = this.Validation.Issuer,
                ValidateAudience = this.Validation.Audience,
                ValidateLifetime = this.Validation.Lifetime,
                ValidateIssuerSigningKey = this.Validation.Key,

                ValidIssuer = this.Issuer,
                ValidAudience = this.Audience,
                IssuerSigningKey = this.GetSecureKey()
            };
        }

        public class JwtValidation {
            public bool Issuer { get; set; } = true;
            public bool Audience { get; set; } = true;
            public bool Lifetime { get; set; } = true;
            public bool Key { get; set; } = true;
        }
    }
}
