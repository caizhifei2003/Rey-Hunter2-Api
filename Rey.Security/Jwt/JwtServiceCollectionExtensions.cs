﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Rey.Security.Jwt;
using System;

namespace Microsoft.Extensions.DependencyInjection {
    public static class JwtServiceCollectionExtensions {
        public static IServiceCollection AddJwtAuthentication(this IServiceCollection services, Action<JwtOptions> configure = null) {
            var jwtOptions = new JwtOptions();
            configure?.Invoke(jwtOptions);

            services.AddSingleton<IJwtOptions>(jwtOptions);
            services.AddSingleton<IJwtTokenGenerator, JwtTokenGenerator>();

            services.AddAuthentication(options => {
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options => {
                options.TokenValidationParameters = jwtOptions.GetValidationParameters();
            });
            return services;
        }
    }
}
