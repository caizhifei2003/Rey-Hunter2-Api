﻿namespace Rey.Security {
    public interface IPasswordHasher {
        (string hashed, string salt) Hash(string password, string salt = null);
    }
}
