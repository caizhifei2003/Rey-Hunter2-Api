﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System;
using System.Security.Cryptography;

namespace Rey.Security {
    public class PasswordHasher : IPasswordHasher {
        private string GenerateSalt() {
            byte[] salt = new byte[128 / 8];
            using (var rng = RandomNumberGenerator.Create()) {
                rng.GetBytes(salt);
            }
            return Convert.ToBase64String(salt);
        }

        public (string hashed, string salt) Hash(string password, string salt = null) {
            if (password == null)
                throw new ArgumentNullException(nameof(password));

            salt = salt ?? this.GenerateSalt();

            byte[] hashed = KeyDerivation.Pbkdf2(
                password: password,
                salt: Convert.FromBase64String(salt),
                prf: KeyDerivationPrf.HMACSHA1,
                iterationCount: 10000,
                numBytesRequested: 256 / 8);

            return (hashed: Convert.ToBase64String(hashed), salt: salt);
        }
    }
}
