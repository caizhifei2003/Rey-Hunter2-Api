﻿using Rey.Security;

namespace Microsoft.Extensions.DependencyInjection {
    public static class SecurityServiceCollectionExtensions {
        public static IServiceCollection AddSecurity(this IServiceCollection services) {
            services.AddJwtAuthentication();
            services.AddSingleton<IPasswordHasher, PasswordHasher>();
            return services;
        }
    }
}
