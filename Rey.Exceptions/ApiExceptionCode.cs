﻿namespace Rey {
    public enum ApiExceptionCode : long {
        [CodeMessage("unknown exception")]
        UNKNOWN = -1,

        [CodeMessage("succeeded")]
        SUCCEEDED = 0,

        [CodeMessage("failed")]
        FAILED,

        #region Empty

        EMPTY_BEGIN = 0x0010,

        [CodeMessage("empty")]
        EMPTY,

        [CodeMessage("empty model")]
        EMPTY_MODEL,

        [CodeMessage("empty id")]
        EMPTY_ID,

        [CodeMessage("empty name")]
        EMPTY_NAME,

        [CodeMessage("empty email")]
        EMPTY_EMAIL,

        [CodeMessage("empty mobile")]
        EMPTY_MOBILE,

        [CodeMessage("empty phone")]
        EMPTY_PHONE,

        [CodeMessage("empty password")]
        EMPTY_PASSWORD,

        [CodeMessage("empty password confirm")]
        EMPTY_PASSWORD_CONFIRM,

        [CodeMessage("empty captcha")]
        EMPTY_CAPTCHA,

        [CodeMessage("empty no")]
        EMPTY_NO,

        EMPTY_END = 0x001F,

        #endregion Empty

        #region Invalid

        INVALID_BEGIN = 0x0020,

        [CodeMessage("invalid")]
        INVALID,

        [CodeMessage("invalid id")]
        INVALID_ID,

        [CodeMessage("invalid name")]
        INVALID_NAME,

        [CodeMessage("invalid email")]
        INVALID_EMAIL,

        [CodeMessage("invalid mobile")]
        INVALID_MOBILE,

        [CodeMessage("invalid phone")]
        INVALID_PHONE,

        [CodeMessage("invalid password")]
        INVALID_PASSWORD,

        [CodeMessage("invalid password confirm")]
        INVALID_PASSWORD_CONFIRM,

        [CodeMessage("invalid captcha")]
        INVALID_CAPTCHA,

        [CodeMessage("invalid no")]
        INVALID_NO,

        INVALID_END = 0x002F,

        #endregion Invalid

        #region Exist

        EXIST_BEGIN = 0x0030,

        [CodeMessage("exist")]
        EXIST,

        [CodeMessage("exist id")]
        EXIST_ID,

        [CodeMessage("exist name")]
        EXIST_NAME,

        [CodeMessage("exist email")]
        EXIST_EMAIL,

        [CodeMessage("exist mobile")]
        EXIST_MOBILE,

        [CodeMessage("exist phone")]
        EXIST_PHONE,

        [CodeMessage("exist user")]
        EXIST_USER,

        [CodeMessage("exist no")]
        EXIST_NO,

        EXIST_END = 0x003F,

        #endregion Exist

        #region NotExist

        NOT_EXIST_BEGIN = 0x0040,

        [CodeMessage("not exist")]
        NOT_EXIST,

        [CodeMessage("not exist id")]
        NOT_EXIST_ID,

        [CodeMessage("not exist name")]
        NOT_EXIST_NAME,

        [CodeMessage("not exist email")]
        NOT_EXIST_EMAIL,

        [CodeMessage("not exist mobile")]
        NOT_EXIST_MOBILE,

        [CodeMessage("not exist phone")]
        NOT_EXIST_PHONE,

        [CodeMessage("not exist user")]
        NOT_EXIST_USER,

        [CodeMessage("exist no")]
        NOT_EXIST_NO,

        NOT_EXIST_END = 0x004F,

        #endregion NotExist

        #region Regular

        REG_BEGIN = 0x0010,

        [CodeMessage("timeout")]
        REG_TIMEOUT,

        [CodeMessage("out of range")]
        REG_OUT_OF_RANGE,

        [CodeMessage("greater than")]
        REG_GT,

        [CodeMessage("greater than equal")]
        REG_GTE,

        [CodeMessage("little than")]
        REG_LT,

        [CodeMessage("little than equal")]
        REG_LTE,

        REG_END = 0x00FF,

        #endregion Regular
    }
}
