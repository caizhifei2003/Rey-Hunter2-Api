﻿using System;
using System.Reflection;

namespace Rey {
    public class ApiException : Exception {
        public ApiExceptionCode Code { get; }
        public ApiException(ApiExceptionCode code, string message)
            : base(message) {
            this.Code = code;
        }

        public static ApiException CreateApiException(ApiExceptionCode code, string message = null) {
            return new ApiException(code, message ?? GetCodeMessage(code));
        }

        public static string GetCodeMessage(ApiExceptionCode code) {
            var type = typeof(ApiExceptionCode);
            return type.GetField(Enum.GetName(type, code))
                .GetCustomAttribute<CodeMessageAttribute>()?
                .Message;
        }

        public static ApiException Unknown(string message = null) => CreateApiException(ApiExceptionCode.UNKNOWN, message);
        public static ApiException Failed(string message = null) => CreateApiException(ApiExceptionCode.FAILED, message);

        public static ApiException Empty(string message = null) => CreateApiException(ApiExceptionCode.EMPTY, message);
        public static ApiException EmptyModel(string message = null) => CreateApiException(ApiExceptionCode.EMPTY_MODEL, message);
        public static ApiException EmptyId(string message = null) => CreateApiException(ApiExceptionCode.EMPTY_ID, message);
        public static ApiException EmptyName(string message = null) => CreateApiException(ApiExceptionCode.EMPTY_NAME, message);
        public static ApiException EmptyEmail(string message = null) => CreateApiException(ApiExceptionCode.EMPTY_EMAIL, message);
        public static ApiException EmptyMobile(string message = null) => CreateApiException(ApiExceptionCode.EMPTY_MOBILE, message);
        public static ApiException EmptyPhone(string message = null) => CreateApiException(ApiExceptionCode.EMPTY_PHONE, message);
        public static ApiException EmptyPassword(string message = null) => CreateApiException(ApiExceptionCode.EMPTY_PASSWORD, message);
        public static ApiException EmptyPasswordConfirm(string message = null) => CreateApiException(ApiExceptionCode.EMPTY_PASSWORD_CONFIRM, message);
        public static ApiException EmptyCaptcha(string message = null) => CreateApiException(ApiExceptionCode.EMPTY_CAPTCHA, message);
        public static ApiException EmptyNo(string message = null) => CreateApiException(ApiExceptionCode.EMPTY_NO, message);

        public static ApiException Invalid(string message = null) => CreateApiException(ApiExceptionCode.INVALID, message);
        public static ApiException InvalidId(string message = null) => CreateApiException(ApiExceptionCode.INVALID_ID, message);
        public static ApiException InvalidName(string message = null) => CreateApiException(ApiExceptionCode.INVALID_NAME, message);
        public static ApiException InvalidEmail(string message = null) => CreateApiException(ApiExceptionCode.INVALID_EMAIL, message);
        public static ApiException InvalidMobile(string message = null) => CreateApiException(ApiExceptionCode.INVALID_MOBILE, message);
        public static ApiException InvalidPhone(string message = null) => CreateApiException(ApiExceptionCode.INVALID_PHONE, message);
        public static ApiException InvalidPassword(string message = null) => CreateApiException(ApiExceptionCode.INVALID_PASSWORD, message);
        public static ApiException InvalidPasswordConfirm(string message = null) => CreateApiException(ApiExceptionCode.INVALID_PASSWORD_CONFIRM, message);
        public static ApiException InvalidCaptcha(string message = null) => CreateApiException(ApiExceptionCode.INVALID_CAPTCHA, message);
        public static ApiException InvalidNo(string message = null) => CreateApiException(ApiExceptionCode.INVALID_NO, message);

        public static ApiException Exist(string message = null) => CreateApiException(ApiExceptionCode.EXIST, message);
        public static ApiException ExistId(string message = null) => CreateApiException(ApiExceptionCode.EXIST_ID, message);
        public static ApiException ExistName(string message = null) => CreateApiException(ApiExceptionCode.EXIST_NAME, message);
        public static ApiException ExistEmail(string message = null) => CreateApiException(ApiExceptionCode.EXIST_EMAIL, message);
        public static ApiException ExistMobile(string message = null) => CreateApiException(ApiExceptionCode.EXIST_MOBILE, message);
        public static ApiException ExistPhone(string message = null) => CreateApiException(ApiExceptionCode.EXIST_PHONE, message);
        public static ApiException ExistUser(string message = null) => CreateApiException(ApiExceptionCode.EXIST_USER, message);
        public static ApiException ExistNo(string message = null) => CreateApiException(ApiExceptionCode.EXIST_NO, message);

        public static ApiException NotExist(string message = null) => CreateApiException(ApiExceptionCode.NOT_EXIST, message);
        public static ApiException NotExistId(string message = null) => CreateApiException(ApiExceptionCode.NOT_EXIST_ID, message);
        public static ApiException NotExistName(string message = null) => CreateApiException(ApiExceptionCode.NOT_EXIST_NAME, message);
        public static ApiException NotExistEmail(string message = null) => CreateApiException(ApiExceptionCode.NOT_EXIST_EMAIL, message);
        public static ApiException NotExistMobile(string message = null) => CreateApiException(ApiExceptionCode.NOT_EXIST_MOBILE, message);
        public static ApiException NotExistPhone(string message = null) => CreateApiException(ApiExceptionCode.NOT_EXIST_PHONE, message);
        public static ApiException NotExistUser(string message = null) => CreateApiException(ApiExceptionCode.NOT_EXIST_USER, message);
        public static ApiException NotExistNo(string message = null) => CreateApiException(ApiExceptionCode.NOT_EXIST_NO, message);

        public static ApiException RegTimeout(string message = null) => CreateApiException(ApiExceptionCode.REG_TIMEOUT, message);
        public static ApiException RegOutOfRange(string message = null) => CreateApiException(ApiExceptionCode.REG_OUT_OF_RANGE, message);
        public static ApiException RegGt(string message = null) => CreateApiException(ApiExceptionCode.REG_GT, message);
        public static ApiException RegGte(string message = null) => CreateApiException(ApiExceptionCode.REG_GTE, message);
        public static ApiException RegLt(string message = null) => CreateApiException(ApiExceptionCode.REG_LT, message);
        public static ApiException RegLte(string message = null) => CreateApiException(ApiExceptionCode.REG_LTE, message);
    }
}
