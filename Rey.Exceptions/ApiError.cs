﻿using System;

namespace Rey {
    public class ApiError {
        public ApiExceptionCode Code { get; }
        public string Message { get; }

        public ApiError(Exception exception) {
            this.Code = (exception as ApiException)?.Code ?? ApiExceptionCode.UNKNOWN;
            this.Message = exception.Message;
        }
    }
}
