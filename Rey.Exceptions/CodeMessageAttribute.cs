﻿using System;

namespace Rey {
    [AttributeUsage(AttributeTargets.Field)]
    public class CodeMessageAttribute : Attribute {
        public string Message { get; }
        public CodeMessageAttribute(string message) {
            this.Message = message;
        }
    }
}
