﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Rey {
    public class DebugApiError : ApiError {
        public IEnumerable<string> Stacks { get; }
        public string Type { get; }

        public DebugApiError(Exception exception)
            : base(exception) {
            this.Stacks = exception.StackTrace.Split('\n').Select(x => x.TrimEnd('\r'));
            this.Type = exception.GetType().FullName;
        }
    }
}
