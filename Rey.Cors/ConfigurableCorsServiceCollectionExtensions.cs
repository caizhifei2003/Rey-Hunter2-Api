﻿using Rey.Cors;
using System;

namespace Microsoft.Extensions.DependencyInjection {
    public static class ConfigurableCorsServiceCollectionExtensions {
        public static IServiceCollection AddConfigurableCors(this IServiceCollection services, Action<ConfigurableCorsOptions> configure) {
            var options = new ConfigurableCorsOptions();
            configure?.Invoke(options);
            services.AddSingleton(options);
            services.AddCors(op => {
                op.AddPolicy("ConfigurableCors", options.ToPolicy());
            });
            return services;
        }
    }
}
