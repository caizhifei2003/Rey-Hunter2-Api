﻿namespace Microsoft.AspNetCore.Builder {
    public static class ConfigurableCorsAppExtensions {
        public static IApplicationBuilder UseConfigurableCors(this IApplicationBuilder app) {
            return app.UseCors("ConfigurableCors");
        }
    }
}
