﻿using Microsoft.AspNetCore.Cors.Infrastructure;
using System.Collections.Generic;

namespace Rey.Cors {
    public class ConfigurableCorsOptions {
        public bool AnyOrigin { get; set; }
        public bool AnyHeader { get; set; }
        public bool AnyMethod { get; set; }
        public List<string> Origins { get; } = new List<string>();
        public List<string> Headers { get; } = new List<string>();
        public List<string> Methods { get; } = new List<string>();

        public CorsPolicy ToPolicy() {
            var builder = new CorsPolicyBuilder();

            if (this.AnyOrigin)
                builder.AllowAnyOrigin();

            if (this.AnyHeader)
                builder.AllowAnyHeader();

            if (this.AnyMethod)
                builder.AllowAnyMethod();

            builder.WithOrigins(this.Origins.ToArray());
            builder.WithHeaders(this.Headers.ToArray());
            builder.WithMethods(this.Methods.ToArray());
            builder.AllowCredentials();

            return builder.Build();
        }
    }
}
