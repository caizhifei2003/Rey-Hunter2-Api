﻿namespace Rey.Hunter.Api.Models.View.Data {
    public class VMLocation : VMEnterpriseNodeModel {
    }

    public class VMLocationNode : VMEnterpriseNode<VMLocationNode> {
    }

    public class VMLocationPageQuery : VMEnterprisePageQuery {
        public string Name { get; set; }
    }
}
