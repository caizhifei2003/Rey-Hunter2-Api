﻿namespace Rey.Hunter.Api.Models.View.Data {
    public class VMCategory : VMEnterpriseNodeModel {
    }

    public class VMCategoryNode : VMEnterpriseNode<VMCategoryNode> {
    }

    public class VMCategoryPageQuery : VMEnterprisePageQuery {
        public string Name { get; set; }
    }
}
