﻿namespace Rey.Hunter.Api.Models.View.Data {
    public class VMChannel : VMEnterpriseNodeModel {
    }

    public class VMChannelNode : VMEnterpriseNode<VMChannelNode> {
    }

    public class VMChannelPageQuery : VMEnterprisePageQuery {
        public string Name { get; set; }
    }
}
