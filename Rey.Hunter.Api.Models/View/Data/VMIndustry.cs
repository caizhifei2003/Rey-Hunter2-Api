﻿namespace Rey.Hunter.Api.Models.View.Data {
    public class VMIndustry : VMEnterpriseNodeModel {
    }

    public class VMIndustryNode : VMEnterpriseNode<VMIndustryNode> {
    }

    public class VMIndustryPageQuery : VMEnterprisePageQuery {
        public string Name { get; set; }
    }
}
