﻿namespace Rey.Hunter.Api.Models.View.Data {
    public class VMFunction : VMEnterpriseNodeModel {
    }

    public class VMFunctionNode : VMEnterpriseNode<VMFunctionNode> {
    }

    public class VMFunctionPageQuery : VMEnterprisePageQuery {
        public string Name { get; set; }
    }
}
