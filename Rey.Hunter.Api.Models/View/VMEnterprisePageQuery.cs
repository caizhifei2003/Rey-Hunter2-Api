﻿using Rey.Mongo.Models.View;

namespace Rey.Hunter.Api.Models.View {
    public class VMEnterprisePageQuery : VMPageQuery {
        public string EnterpriseId { get; set; }
    }
}
