﻿namespace Rey.Hunter.Api.Models.View.Business {
    public class VMTalent : VMEnterpriseModel {
        public string EnglishName { get; set; }
        public string ChineseName { get; set; }
    }
}
