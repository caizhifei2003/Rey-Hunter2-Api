﻿using System.Collections.Generic;

namespace Rey.Hunter.Api.Models.View.Business {
    public class VMCompanyQuery : VMEnterprisePageQuery {
        public List<string> Name { get; set; } = new List<string>();
    }
}
