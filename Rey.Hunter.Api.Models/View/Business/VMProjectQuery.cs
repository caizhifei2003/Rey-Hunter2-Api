﻿using System.Collections.Generic;

namespace Rey.Hunter.Api.Models.View.Business {
    public class VMProjectQuery : VMEnterprisePageQuery {
        public List<string> Name { get; set; } = new List<string>();
    }
}
