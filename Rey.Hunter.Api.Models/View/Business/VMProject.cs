﻿namespace Rey.Hunter.Api.Models.View.Business {
    public class VMProject : VMEnterpriseModel {
        public string Name { get; set; }
        public VMCompany Client { get; set; }
    }
}
