﻿namespace Rey.Hunter.Api.Models.View.Business {
    public class VMCompany : VMEnterpriseModel {
        public string Name { get; set; }
    }
}
