﻿using System.Collections.Generic;

namespace Rey.Hunter.Api.Models.View.Business {
    public class VMTalentQuery : VMEnterprisePageQuery {
        public List<string> EnglishName { get; set; } = new List<string>();
        public List<string> ChineseName { get; set; } = new List<string>();
    }
}
