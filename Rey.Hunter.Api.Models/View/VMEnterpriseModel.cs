﻿using Rey.Mongo.Models.View;

namespace Rey.Hunter.Api.Models.View {
    public abstract class VMEnterpriseModel : VMModel {
        public string EnterpriseId { get; set; }
    }
}
