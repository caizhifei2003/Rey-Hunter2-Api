﻿using Rey.Mongo.Models.View;

namespace Rey.Hunter.Api.Models.View.Account {
    public class VMEnterprise : VMModel {
        public string Name { get; set; }
    }
}
