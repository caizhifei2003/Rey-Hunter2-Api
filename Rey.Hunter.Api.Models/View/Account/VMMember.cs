﻿namespace Rey.Hunter.Api.Models.View.Account {
    public class VMMember {
        public string Email { get; set; }
        public string Name { get; set; }
    }

    public class VMMemberSignUp {
        public string Email { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
    }

    public class VMMemberSignIn {
        public string Email { get; set; }
        public string Password { get; set; }
        public bool Remember { get; set; }
    }

    public class VMMemberSignInEnterprise {
        public string MemberId { get; set; }
        public string EnterpriseId { get; set; }
        public long ExpiresIn { get; set; } = 7200;
    }

    public class VMMemberToken {
        public string Token { get; set; }
        public double ExpiresIn { get; set; }
        public long Timestamp { get; set; }
    }
}
