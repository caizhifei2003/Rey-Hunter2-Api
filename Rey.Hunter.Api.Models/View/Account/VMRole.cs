﻿using System.Collections.Generic;

namespace Rey.Hunter.Api.Models.View.Account {
    public class VMRole : VMEnterpriseModel {
        public string Name { get; set; }
        public List<string> Permissions { get; set; } = new List<string>();
        public List<string> Members { get; set; } = new List<string>();
    }
}
