﻿using Rey.Mongo.Models.View;

namespace Rey.Hunter.Api.Models.View {
    public abstract class VMEnterpriseNodeModel : VMNodeModel {
        public string EnterpriseId { get; set; }
    }
}
