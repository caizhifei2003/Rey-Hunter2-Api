﻿using Rey.Mongo.Models.View;

namespace Rey.Hunter.Api.Models.View {
    public abstract class VMEnterpriseNode<T> : VMNode<T>
        where T : VMEnterpriseNode<T> {
        public string EnterpriseId { get; set; }
    }
}
