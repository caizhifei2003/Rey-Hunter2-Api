﻿using Rey.Mongo.Attributes;
using Rey.Mongo.Models.Data;

namespace Rey.Hunter.Api.Models.Data.Account {
    [Collection("account.enterprise")]
    public class DEnterprise : DModel {
        public string Name { get; set; }
    }
}
