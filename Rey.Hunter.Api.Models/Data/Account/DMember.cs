﻿using Rey.Mongo.Attributes;
using Rey.Mongo.Models.Data;

namespace Rey.Hunter.Api.Models.Data.Account {
    [Collection("account.member")]
    public class DMember : DModel {
        public string Email { get; set; }
        public string Name { get; set; }
        public string HashedPassword { get; set; }
        public string HashedSalt { get; set; }
    }
}
