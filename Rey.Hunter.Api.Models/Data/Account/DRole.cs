﻿using Rey.Mongo.Attributes;
using System.Collections.Generic;

namespace Rey.Hunter.Api.Models.Data.Account {
    [Collection("account.role")]
    public class DRole : DEnterpriseModel {
        public string Name { get; set; }
        public List<string> Permissions { get; set; } = new List<string>();
        public List<string> Members { get; set; } = new List<string>();
    }
}
