﻿using Rey.Mongo.Models.Data;

namespace Rey.Hunter.Api.Models.Data {
    public abstract class DEnterpriseModel : DModel {
        public string EnterpriseId { get; set; }
    }
}
