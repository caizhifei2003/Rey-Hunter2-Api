﻿using Rey.Mongo.Models.Data;

namespace Rey.Hunter.Api.Models.Data {
    public abstract class DEnterpriseNodeModel : DNodeModel {
        public string EnterpriseId { get; set; }
    }
}
