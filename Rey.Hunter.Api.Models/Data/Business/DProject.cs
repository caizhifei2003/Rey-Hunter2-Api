﻿using Rey.Hunter.Api.Models.View.Business;
using Rey.Mongo.Attributes;
using Rey.Mongo.Models;

namespace Rey.Hunter.Api.Models.Data.Business {
    [Collection("business.project")]
    public class DProject : DEnterpriseModel {
        public string Name { get; set; }
        public DCompanyRef Client { get; set; }
    }

    public class DProjectRef : RefModel<DProject, VMProject> {
        public string Name { get; set; }
    }
}
