﻿using Rey.Hunter.Api.Models.View.Business;
using Rey.Mongo.Attributes;
using Rey.Mongo.Models;

namespace Rey.Hunter.Api.Models.Data.Business {
    [Collection("business.talent")]
    public class DTalent : DEnterpriseModel {
        public string EnglishName { get; set; }
        public string ChineseName { get; set; }
    }

    public class DTalentRef : RefModel<DTalent, VMTalent> {
        public string EnglishName { get; set; }
        public string ChineseName { get; set; }
    }
}
