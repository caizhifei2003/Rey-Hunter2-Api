﻿using Rey.Hunter.Api.Models.View.Business;
using Rey.Mongo.Attributes;
using Rey.Mongo.Models;

namespace Rey.Hunter.Api.Models.Data.Business {
    [Collection("business.company")]
    public class DCompany : DEnterpriseModel {
        public string Name { get; set; }
    }

    public class DCompanyRef : RefModel<DCompany, VMCompany> {
        public string Name { get; set; }
    }
}
