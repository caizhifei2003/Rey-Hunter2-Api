﻿using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace Rey.Hunter.Api {
    public static class ControllerExtensions {
        public static string GetMemberId(this ControllerBase controller) {
            return controller.User.FindFirstValue(ClaimTypes.NameIdentifier);
        }

        public static string GetEnterpriseId(this ControllerBase controller) {
            return controller.User.FindFirstValue("e_id");
        }
    }
}
