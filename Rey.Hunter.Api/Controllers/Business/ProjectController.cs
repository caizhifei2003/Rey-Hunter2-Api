﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Rey.Hunter.Api.Models.Data.Business;
using Rey.Hunter.Api.Models.View.Business;
using Rey.Mongo;

namespace Rey.Hunter.Api.Controllers.Business {
    [Authorize]
    [Route("[controller]")]
    public class ProjectController : ModelController<DProject, VMProject, VMProjectQuery> {
        public ProjectController(IModelService<DProject, VMProject, VMProjectQuery> service)
            : base(service) {
        }
    }
}
