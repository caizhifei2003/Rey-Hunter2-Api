﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Rey.Hunter.Api.Models.Data.Business;
using Rey.Hunter.Api.Models.View.Business;
using Rey.Mongo;

namespace Rey.Hunter.Api.Controllers.Business {
    [Authorize]
    [Route("[controller]")]
    public class CompanyController : ModelController<DCompany, VMCompany, VMCompanyQuery> {
        public CompanyController(IModelService<DCompany, VMCompany, VMCompanyQuery> service)
            : base(service) {
        }
    }
}
