﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Rey.Hunter.Api.Models.Data.Data;
using Rey.Hunter.Api.Models.View.Data;
using Rey.Mongo;

namespace Rey.Hunter.Api.Controllers.Data {
    [Authorize]
    [Route("[controller]")]
    public class LocationController : NodeController<DLocation, VMLocation, VMLocationPageQuery, VMLocationNode> {
        public LocationController(INodeService<DLocation, VMLocation, VMLocationPageQuery, VMLocationNode> service)
            : base(service) {
        }
    }
}
