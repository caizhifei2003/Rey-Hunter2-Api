﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Rey.Hunter.Api.Models.Data.Data;
using Rey.Hunter.Api.Models.View.Data;
using Rey.Mongo;

namespace Rey.Hunter.Api.Controllers.Data {
    [Authorize]
    [Route("[controller]")]
    public class FunctionController : NodeController<DFunction, VMFunction, VMFunctionPageQuery, VMFunctionNode> {
        public FunctionController(INodeService<DFunction, VMFunction, VMFunctionPageQuery, VMFunctionNode> service)
            : base(service) {
        }
    }
}
