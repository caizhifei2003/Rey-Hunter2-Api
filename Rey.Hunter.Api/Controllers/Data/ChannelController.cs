﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Rey.Hunter.Api.Models.Data.Data;
using Rey.Hunter.Api.Models.View.Data;
using Rey.Mongo;

namespace Rey.Hunter.Api.Controllers.Data {
    [Authorize]
    [Route("[controller]")]
    public class ChannelController : NodeController<DChannel, VMChannel, VMChannelPageQuery, VMChannelNode> {
        public ChannelController(INodeService<DChannel, VMChannel, VMChannelPageQuery, VMChannelNode> service)
            : base(service) {
        }
    }
}
