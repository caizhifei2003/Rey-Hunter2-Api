﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Rey.Hunter.Api.Models.Data.Data;
using Rey.Hunter.Api.Models.View.Data;
using Rey.Mongo;

namespace Rey.Hunter.Api.Controllers.Data {
    [Authorize]
    [Route("[controller]")]
    public class CategoryController : NodeController<DCategory, VMCategory, VMCategoryPageQuery, VMCategoryNode> {
        public CategoryController(INodeService<DCategory, VMCategory, VMCategoryPageQuery, VMCategoryNode> service)
            : base(service) {
        }
    }
}
