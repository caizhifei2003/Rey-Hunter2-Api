﻿using Microsoft.AspNetCore.Mvc;
using Rey.Hunter.Api.Models.Data;
using Rey.Hunter.Api.Models.View;
using Rey.Mongo;
using Rey.Mongo.Models.View;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Rey.Hunter.Api.Controllers.Business {
    public class ModelController<TDModel, TVMModel, TQuery> : Controller
        where TDModel : DEnterpriseModel
        where TVMModel : VMEnterpriseModel
        where TQuery : VMEnterprisePageQuery {

        private IModelService<TDModel, TVMModel, TQuery> Service { get; }

        public ModelController(IModelService<TDModel, TVMModel, TQuery> service) {
            this.Service = service;
        }

        [HttpPost]
        public async Task<TVMModel> CreateAsync([FromBody]TVMModel model) {
            model.EnterpriseId = this.GetEnterpriseId();
            return await this.Service.CreateAsync(model);
        }

        [HttpPut("{id}")]
        public async Task<TVMModel> UpdateAsync(string id, [FromBody]TVMModel model) {
            return await this.Service.UpdateAsync(id, model);
        }

        [HttpDelete("{id}")]
        public async Task<TVMModel> DeleteAsync(string id) {
            return await this.Service.DeleteAsync(id);
        }

        [HttpDelete]
        public async Task<IEnumerable<TVMModel>> BatchDeleteAsync(string[] ids) {
            return await this.Service.BatchDeleteAsync(ids);
        }

        [HttpGet]
        public async Task<IEnumerable<TVMModel>> QueryAsync(TQuery query) {
            query.EnterpriseId = this.GetEnterpriseId();
            return await this.Service.QueryAsync(query);
        }

        [HttpGet("page")]
        public async Task<VMPageData<TVMModel>> PageQueryAscyn(TQuery query) {
            query.EnterpriseId = this.GetEnterpriseId();
            return await this.Service.PageQueryAsync(query);
        }
    }
}
