﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Rey.Hunter.Api.Models.View.Account;
using Rey.Hunter.Api.Services.Account;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Rey.Hunter.Api.Controllers {
    [Authorize]
    [Route("[controller]")]
    public class EnterpriseController : Controller {
        private IEnterprise Enterprise { get; }

        public EnterpriseController(IEnterprise enterprise) {
            this.Enterprise = enterprise;
        }

        [HttpPost]
        public async Task<VMEnterprise> CreateAsync([FromBody]VMEnterprise model) {
            return await this.Enterprise.CreateAsync(this.GetMemberId(), model);
        }

        [HttpPut("{id}")]
        public async Task<VMEnterprise> UpdateAsync(string id, [FromBody]VMEnterprise model) {
            return await this.Enterprise.UpdateAsync(id, model);
        }

        [HttpDelete("{id}")]
        public async Task<VMEnterprise> DeleteAsync(string id) {
            return await this.Enterprise.DeleteAsync(id);
        }

        [HttpGet("all")]
        public async Task<IEnumerable<VMEnterprise>> QueryAllAsync() {
            return await this.Enterprise.QueryAll(this.GetMemberId());
        }
    }
}
