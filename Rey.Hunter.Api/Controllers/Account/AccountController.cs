﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Rey.Hunter.Api.Models.View.Account;
using Rey.Hunter.Api.Services.Account;
using System.Threading.Tasks;

namespace Rey.Hunter.Api.Controllers {
    [Route("[controller]")]
    public class AccountController : Controller {
        private IAccount Account { get; }

        public AccountController(IAccount account) {
            this.Account = account;
        }

        [HttpPost("signup")]
        public async Task<VMMember> SignUpAsync([FromBody]VMMemberSignUp model) {
            return await this.Account.SignUpAsync(model);
        }

        [HttpPost("signin")]
        public async Task<VMMemberToken> SignInAsync([FromBody]VMMemberSignIn model) {
            return await this.Account.SignInAsync(model);
        }

        [Authorize]
        [HttpPost("signin/enterprise")]
        public async Task<VMMemberToken> SignInEnterpriseAsync([FromBody]VMMemberSignInEnterprise model) {
            model.MemberId = this.GetMemberId();
            return await this.Account.SignInEnterpriseAsync(model);
        }

        [Authorize]
        [HttpGet("member")]
        public async Task<VMMember> GetMemberAsync() {
            var id = this.GetMemberId();
            return await this.Account.GetMemberAsync(id);
        }
    }
}
