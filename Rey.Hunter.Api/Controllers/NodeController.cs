﻿using Microsoft.AspNetCore.Mvc;
using Rey.Hunter.Api.Models.Data;
using Rey.Hunter.Api.Models.View;
using Rey.Mongo;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Rey.Hunter.Api.Controllers {
    public abstract class NodeController<TDModel, TVMModel, TQuery, TNode> : Controller
        where TDModel : DEnterpriseNodeModel
        where TVMModel : VMEnterpriseNodeModel
        where TQuery : VMEnterprisePageQuery
        where TNode : VMEnterpriseNode<TNode> {

        private INodeService<TDModel, TVMModel, TQuery, TNode> Service { get; }

        public NodeController(
            INodeService<TDModel, TVMModel, TQuery, TNode> service
            ) {
            this.Service = service;
        }

        [HttpPost]
        public async Task<TVMModel> CreateAsync([FromBody]TVMModel model) {
            model.EnterpriseId = this.GetEnterpriseId();
            return await this.Service.CreateAsync(model);
        }

        [HttpPut("{id}")]
        public async Task<TVMModel> UpdateAsync(string id, [FromBody]TVMModel model) {
            return await this.Service.UpdateAsync(id, model);
        }

        [HttpDelete("{id}")]
        public async Task<TVMModel> DeleteAsync(string id) {
            return await this.Service.DeleteAsync(id);
        }

        [HttpGet("nodes")]
        public async Task<IEnumerable<TNode>> QueryNodesAsync(TQuery query) {
            query.EnterpriseId = this.GetEnterpriseId();
            return await this.Service.QueryNodesAsync(query);
        }
    }
}
