﻿using AutoMapper;
using Rey.Mongo;
using System.Collections.Generic;

namespace Rey.Hunter.Api.Mapping {
    public class ModelMapper : IModelMapper {
        private IMapper Mapper { get; }

        public ModelMapper(IMapper mapper) {
            this.Mapper = mapper;
        }

        public T Map<T>(object from) {
            return this.Mapper.Map<T>(from);
        }

        public IEnumerable<T> MapCollection<T>(object from) {
            return this.Mapper.Map<IEnumerable<T>>(from);
        }
    }
}
