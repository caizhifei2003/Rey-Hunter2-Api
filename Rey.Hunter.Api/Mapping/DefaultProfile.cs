﻿using AutoMapper;
using Rey.Hunter.Api.Models.Data.Data;
using Rey.Hunter.Api.Models.View.Data;

namespace Rey.Hunter.Api.Mapping {
    public class DefaultProfile : Profile {
        public DefaultProfile() {
            this.CreateMap<DCategory, VMCategoryNode>();
            this.CreateMap<DChannel, VMChannelNode>();
            this.CreateMap<DFunction, VMFunctionNode>();
            this.CreateMap<DIndustry, VMIndustryNode>();
            this.CreateMap<DLocation, VMLocationNode>();
        }
    }
}
