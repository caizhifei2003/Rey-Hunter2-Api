﻿using Rey.Hunter.Api.Models.Data.Business;
using Rey.Hunter.Api.Models.View.Business;
using Rey.Mongo;
using Rey.Mongo.Builders;

namespace Rey.Hunter.Api.Services.Business {
    public class Talent : ModelConfig<DTalent, VMTalent, VMTalentQuery> {
        public override void GenerateQueryFilter(IFilterBuilder<DTalent> builder, VMTalentQuery query) {
            builder.Eq(x => x.EnterpriseId, query.EnterpriseId);
            builder.OrLike(x => x.EnglishName, query.EnglishName);
            builder.OrLike(x => x.ChineseName, query.ChineseName);
        }

        public override void GenerateQuerySort(ISortBuilder<DTalent> builder, VMTalentQuery query) {
            builder.Sort(query.SortBy, query.Sort, b => b
                .On(x => x.EnglishName)
                .On(x => x.ChineseName)
            );
        }

        public override void GenerateUpdate(IUpdateBuilder<DTalent> builder, string id, VMTalent model) {
            builder.Set(x => x.EnglishName, model.EnglishName);
            builder.Set(x => x.ChineseName, model.ChineseName);
        }
    }
}
