﻿using MongoDB.Driver;
using Rey.Hunter.Api.Models.Data.Business;
using Rey.Hunter.Api.Models.View.Business;
using Rey.Mongo;
using Rey.Mongo.Builders;
using Rey.Mongo.Helpers;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rey.Hunter.Api.Services.Business {
    public class Project : ModelConfig<DProject, VMProject, VMProjectQuery> {
        public override void GenerateUpdate(IUpdateBuilder<DProject> builder, string id, VMProject model) {
            builder.Set(x => x.Name, model.Name);
        }

        public override void GenerateQueryFilter(IFilterBuilder<DProject> builder, VMProjectQuery query) {
            builder.Eq(x => x.EnterpriseId, query.EnterpriseId);
            builder.OrLike(x => x.Name, query.Name);
        }

        public override void GenerateQuerySort(ISortBuilder<DProject> builder, VMProjectQuery query) {
            builder.Sort(query.SortBy, query.Sort, b => b
                .On(x => x.Name)
            );
        }

        public override async Task<VMProject> AttachAsync(IAttachHelper<DProject, VMProject> helper, DProject from, VMProject to) {
            to.Client = await helper.RefAsync(from.Client);
            return to;
        }

        public override async Task<IEnumerable<VMProject>> AttachAsync(IAttachHelper<DProject, VMProject> helper, IEnumerable<DProject> from, IEnumerable<VMProject> to) {
            var clients = await helper.RefAsync(from.Select(x => x.Client));
            return to.Select(x => {
                if (x.Client == null)
                    return x;

                var client = clients.FirstOrDefault(y => y.Id == x.Client.Id);
                if (client == null)
                    return x;

                x.Client = client;
                return x;
            });
        }
    }
}
