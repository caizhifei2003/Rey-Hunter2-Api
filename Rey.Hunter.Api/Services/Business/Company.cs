﻿using Rey.Hunter.Api.Models.Data.Business;
using Rey.Hunter.Api.Models.View.Business;
using Rey.Mongo;
using Rey.Mongo.Builders;

namespace Rey.Hunter.Api.Services.Business {
    public class Company : ModelConfig<DCompany, VMCompany, VMCompanyQuery> {
        public override void GenerateUpdate(Mongo.Builders.IUpdateBuilder<DCompany> builder, string id, VMCompany model) {
            builder.Set(x => x.Name, model.Name);
        }

        public override void GenerateQueryFilter(IFilterBuilder<DCompany> builder, VMCompanyQuery query) {
            builder.Eq(x => x.EnterpriseId, query.EnterpriseId);
            builder.OrLike(x => x.Name, query.Name);
        }

        public override void GenerateQuerySort(ISortBuilder<DCompany> builder, VMCompanyQuery query) {
            builder.Sort(query.SortBy, query.Sort, b => b.On(x => x.Name));
        }
    }
}
