﻿using Rey.Hunter.Api.Models.Data.Data;
using Rey.Hunter.Api.Models.View.Data;
using Rey.Mongo;
using Rey.Mongo.Builders;

namespace Rey.Hunter.Api.Services.Data {
    public class Function : NodeConfig<DFunction, VMFunction, VMFunctionPageQuery, VMFunctionNode> {
        public override void GenerateQueryFilter(IFilterBuilder<DFunction> builder, VMFunctionPageQuery query) {
            builder.Eq(x => x.EnterpriseId, query.EnterpriseId);

            if (query.Name != null)
                builder.Eq(x => x.Name, query.Name);
        }

        public override void GenerateQuerySort(ISortBuilder<DFunction> builder, VMFunctionPageQuery query) {

        }

        public override void GenerateUpdate(IUpdateBuilder<DFunction> builder, string id, VMFunction model) {
            builder.Set(x => x.Name, model.Name);
        }
    }
}
