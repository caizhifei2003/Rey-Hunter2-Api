﻿using Rey.Hunter.Api.Models.Data.Data;
using Rey.Hunter.Api.Models.View.Data;
using Rey.Mongo;
using Rey.Mongo.Builders;

namespace Rey.Hunter.Api.Services.Data {
    public class Location : NodeConfig<DLocation, VMLocation, VMLocationPageQuery, VMLocationNode> {
        public override void GenerateQueryFilter(IFilterBuilder<DLocation> builder, VMLocationPageQuery query) {
            builder.Eq(x => x.EnterpriseId, query.EnterpriseId);

            if (query.Name != null)
                builder.Eq(x => x.Name, query.Name);
        }

        public override void GenerateQuerySort(ISortBuilder<DLocation> builder, VMLocationPageQuery query) {

        }

        public override void GenerateUpdate(IUpdateBuilder<DLocation> builder, string id, VMLocation model) {
            builder.Set(x => x.Name, model.Name);
        }
    }
}
