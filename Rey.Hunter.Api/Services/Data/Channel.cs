﻿using Rey.Hunter.Api.Models.Data.Data;
using Rey.Hunter.Api.Models.View.Data;
using Rey.Mongo;
using Rey.Mongo.Builders;

namespace Rey.Hunter.Api.Services.Data {
    public class Channel : NodeConfig<DChannel, VMChannel, VMChannelPageQuery, VMChannelNode> {
        public override void GenerateQueryFilter(IFilterBuilder<DChannel> builder, VMChannelPageQuery query) {
            builder.Eq(x => x.EnterpriseId, query.EnterpriseId);

            if (query.Name != null)
                builder.Eq(x => x.Name, query.Name);
        }

        public override void GenerateQuerySort(ISortBuilder<DChannel> builder, VMChannelPageQuery query) {

        }

        public override void GenerateUpdate(IUpdateBuilder<DChannel> builder, string id, VMChannel model) {
            builder.Set(x => x.Name, model.Name);
        }
    }
}
