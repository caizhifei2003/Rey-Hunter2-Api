﻿using Rey.Hunter.Api.Models.Data.Data;
using Rey.Hunter.Api.Models.View.Data;
using Rey.Mongo;
using Rey.Mongo.Builders;

namespace Rey.Hunter.Api.Services.Data {
    public class Industry : NodeConfig<DIndustry, VMIndustry, VMIndustryPageQuery, VMIndustryNode> {
        public override void GenerateQueryFilter(IFilterBuilder<DIndustry> builder, VMIndustryPageQuery query) {
            builder.Eq(x => x.EnterpriseId, query.EnterpriseId);

            if (query.Name != null)
                builder.Eq(x => x.Name, query.Name);
        }

        public override void GenerateQuerySort(ISortBuilder<DIndustry> builder, VMIndustryPageQuery query) {

        }

        public override void GenerateUpdate(IUpdateBuilder<DIndustry> builder, string id, VMIndustry model) {
            builder.Set(x => x.Name, model.Name);
        }
    }
}
