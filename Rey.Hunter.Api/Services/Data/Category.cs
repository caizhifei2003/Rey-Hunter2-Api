﻿using Rey.Hunter.Api.Models.Data.Data;
using Rey.Hunter.Api.Models.View.Data;
using Rey.Mongo;
using Rey.Mongo.Builders;

namespace Rey.Hunter.Api.Services.Data {
    public class Category : NodeConfig<DCategory, VMCategory, VMCategoryPageQuery, VMCategoryNode> {
        public override void GenerateQueryFilter(IFilterBuilder<DCategory> builder, VMCategoryPageQuery query) {
            builder.Eq(x => x.EnterpriseId, query.EnterpriseId);

            if (query.Name != null)
                builder.Eq(x => x.Name, query.Name);
        }

        public override void GenerateQuerySort(ISortBuilder<DCategory> builder, VMCategoryPageQuery query) {

        }

        public override void GenerateUpdate(IUpdateBuilder<DCategory> builder, string id, VMCategory model) {
            builder.Set(x => x.Name, model.Name);
        }
    }
}
