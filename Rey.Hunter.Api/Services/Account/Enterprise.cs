﻿using AutoMapper;
using MongoDB.Driver;
using Rey.Hunter.Api.Models.Data.Account;
using Rey.Hunter.Api.Models.View.Account;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rey.Hunter.Api.Services.Account {
    public class Enterprise : IEnterprise {
        private IMapper Mapper { get; }
        private IMongoCollection<DEnterprise> Repo { get; }
        private IMongoCollection<DRole> RepoRole { get; }

        public Enterprise(
            IMapper mapper,
            IMongoCollection<DEnterprise> repo,
            IMongoCollection<DRole> repoRole
            ) {
            this.Mapper = mapper;
            this.Repo = repo;
            this.RepoRole = repoRole;
        }

        public async Task<VMEnterprise> CreateAsync(string memberId, VMEnterprise model) {
            var dEnterprise = this.Mapper.Map<DEnterprise>(model);
            await this.Repo.InsertOneAsync(dEnterprise);

            var dRole = new DRole {
                Name = "Administrators",
                EnterpriseId = dEnterprise.Id,
                Permissions = Constant.Permission,
                Members = new List<string> { memberId },
            };
            await this.RepoRole.InsertOneAsync(dRole);

            return this.Mapper.Map<VMEnterprise>(dEnterprise);
        }

        public async Task<VMEnterprise> UpdateAsync(string id, VMEnterprise model) {
            var filter = Builders<DEnterprise>.Filter.Eq(x => x.Id, id);
            var update = Builders<DEnterprise>.Update.Set(x => x.Name, model.Name);
            var ret = await this.Repo.UpdateOneAsync(filter, update);
            if (ret.MatchedCount == 0)
                return null;

            var dModel = await this.Repo.Find(filter).FirstOrDefaultAsync();
            return this.Mapper.Map<VMEnterprise>(dModel);
        }

        public async Task<IEnumerable<VMEnterprise>> QueryAll(string memberId) {
            var filterRole = Builders<DRole>.Filter.Eq(x => x.Members[0], memberId);
            var dRoles = await this.RepoRole.Find(filterRole).ToListAsync();
            var enterpriseIds = dRoles.Select(x => x.EnterpriseId).ToList();
            var filter = Builders<DEnterprise>.Filter.In(x => x.Id, enterpriseIds);
            var dModels = await this.Repo.Find(filter).ToListAsync();
            return this.Mapper.Map<IEnumerable<VMEnterprise>>(dModels);
        }

        public async Task<VMEnterprise> DeleteAsync(string id) {
            var filter = Builders<DEnterprise>.Filter.Eq(x => x.Id, id);
            var dModel = await this.Repo.FindOneAndDeleteAsync(filter);
            if (dModel == null)
                return null;
            return this.Mapper.Map<VMEnterprise>(dModel);
        }
    }
}
