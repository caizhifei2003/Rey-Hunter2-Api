﻿using Rey.Hunter.Api.Models.View.Account;
using System.Threading.Tasks;

namespace Rey.Hunter.Api.Services.Account {
    public interface IAccount {
        Task<VMMember> SignUpAsync(VMMemberSignUp model);
        Task<VMMemberToken> SignInAsync(VMMemberSignIn model);
        Task<VMMemberToken> SignInEnterpriseAsync(VMMemberSignInEnterprise model);
        Task<VMMember> GetMemberAsync(string id);
    }
}
