﻿using AutoMapper;
using MongoDB.Driver;
using Rey.Hunter.Api.Models.Data.Account;
using Rey.Hunter.Api.Models.View.Account;
using Rey.Security;
using Rey.Security.Jwt;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Rey.Hunter.Api.Services.Account {
    public class Account : IAccount {
        private IMapper Mapper { get; }
        private IMongoCollection<DMember> Repo { get; }
        private IMongoCollection<DEnterprise> RepoEnterprise { get; }
        private IMongoCollection<DRole> RepoRole { get; }
        private IPasswordHasher Hasher { get; }
        private IJwtTokenGenerator TokenGenerator { get; }

        public Account(
            IMapper mapper,
            IMongoCollection<DMember> repo,
            IMongoCollection<DEnterprise> repoEnterprise,
            IMongoCollection<DRole> repoRole,
            IPasswordHasher hasher,
            IJwtTokenGenerator tokenGenerator
            ) {
            this.Mapper = mapper;
            this.Repo = repo;
            this.RepoEnterprise = repoEnterprise;
            this.RepoRole = repoRole;
            this.Hasher = hasher;
            this.TokenGenerator = tokenGenerator;
        }

        public async Task<VMMember> SignUpAsync(VMMemberSignUp model) {
            if (string.IsNullOrEmpty(model.Email))
                throw ApiException.EmptyEmail();

            if (string.IsNullOrEmpty(model.Password))
                throw ApiException.EmptyPassword();

            var count = await this.Repo.Find(x => x.Email == model.Email).CountDocumentsAsync();
            if (count > 0)
                throw ApiException.ExistEmail();

            var (pwd, salt) = this.Hasher.Hash(model.Password);
            var dMember = new DMember {
                Email = model.Email,
                HashedPassword = pwd,
                HashedSalt = salt,
            };

            await this.Repo.InsertOneAsync(dMember);
            return this.Mapper.Map<VMMember>(dMember);
        }

        public async Task<VMMemberToken> SignInAsync(VMMemberSignIn model) {
            if (string.IsNullOrEmpty(model.Email))
                throw ApiException.EmptyEmail();

            if (string.IsNullOrEmpty(model.Password))
                throw ApiException.EmptyPassword();

            var dMember = await this.Repo.Find(x => x.Email == model.Email).FirstOrDefaultAsync();
            if (dMember == null)
                throw ApiException.InvalidEmail();

            var (hashed, _) = this.Hasher.Hash(model.Password, dMember.HashedSalt);
            if (!dMember.HashedPassword.Equals(hashed))
                throw ApiException.InvalidPassword();

            var claims = new List<Claim> {
                new Claim(JwtRegisteredClaimNames.Sub, dMember.Id),
                new Claim(JwtRegisteredClaimNames.Email, dMember.Email)
            };

            var token = this.TokenGenerator.Generate(claims, model.Remember ? 86400 : 7200);

            return new VMMemberToken {
                Token = token.ToString(),
                Timestamp = token.Timestamp,
                ExpiresIn = token.ExpiresIn,
            };
        }

        public async Task<VMMemberToken> SignInEnterpriseAsync(VMMemberSignInEnterprise model) {
            var dMember = await this.Repo.Find(x => x.Id == model.MemberId).FirstOrDefaultAsync();
            if (dMember == null)
                throw ApiException.InvalidId($"invalid member id. {model.MemberId}");

            var dEnterprise = await this.RepoEnterprise.Find(x => x.Id == model.EnterpriseId).FirstOrDefaultAsync();
            if (dEnterprise == null)
                throw ApiException.InvalidId($"invalid enterprise id. {model.EnterpriseId}");

            var dRoles = await this.RepoRole.Find(x => x.EnterpriseId == model.EnterpriseId).ToListAsync();
            var permissions = dRoles.SelectMany(x => x.Permissions).Distinct().ToList();

            var claims = new List<Claim> {
                new Claim(JwtRegisteredClaimNames.Sub, dMember.Id),
                new Claim(JwtRegisteredClaimNames.Email, dMember.Email),
                new Claim("e_id", dEnterprise.Id),
                new Claim("e_name", dEnterprise.Name),
                new Claim("permission", $"{string.Join(",", permissions)}"),
            };

            var token = this.TokenGenerator.Generate(claims, model.ExpiresIn);

            return new VMMemberToken {
                Token = token.ToString(),
                Timestamp = token.Timestamp,
                ExpiresIn = token.ExpiresIn,
            };
        }

        public async Task<VMMember> GetMemberAsync(string id) {
            var dMember = await this.Repo.Find(x => x.Id == id).FirstOrDefaultAsync();
            if (dMember == null)
                return null;

            return this.Mapper.Map<VMMember>(dMember);
        }
    }
}
