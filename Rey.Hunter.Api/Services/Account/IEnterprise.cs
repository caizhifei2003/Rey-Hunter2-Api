﻿using Rey.Hunter.Api.Models.View.Account;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Rey.Hunter.Api.Services.Account {
    public interface IEnterprise {
        Task<VMEnterprise> CreateAsync(string memberId, VMEnterprise model);
        Task<IEnumerable<VMEnterprise>> QueryAll(string memberId);
        Task<VMEnterprise> UpdateAsync(string id, VMEnterprise model);
        Task<VMEnterprise> DeleteAsync(string id);
    }
}
