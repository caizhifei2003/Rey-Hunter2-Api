﻿using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Rey.Hunter.Api.Mapping;
using Rey.Hunter.Api.Models.Data.Business;
using Rey.Hunter.Api.Models.Data.Data;
using Rey.Hunter.Api.Models.View.Business;
using Rey.Hunter.Api.Models.View.Data;
using Rey.Hunter.Api.Services.Account;
using Rey.Hunter.Api.Services.Business;
using Rey.Hunter.Api.Services.Data;

namespace Rey.Hunter.Api {
    public class Startup {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration) {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services) {
            //! 添加MVC
            services.AddMvc(options => {
                options.ReyApi();
            });

            //! 添加cors服务
            services.AddConfigurableCors(options => this.Configuration.GetSection("cors").Bind(options));

            //! 添加mongodb数据库
            services.AddMongo(options => this.Configuration.GetSection("mongodb").Bind(options));

            services.AddMongoApp()
                .Mapper<ModelMapper>()
                .NodeConfig<Category, DCategory, VMCategory, VMCategoryPageQuery, VMCategoryNode>()
                .NodeConfig<Channel, DChannel, VMChannel, VMChannelPageQuery, VMChannelNode>()
                .NodeConfig<Function, DFunction, VMFunction, VMFunctionPageQuery, VMFunctionNode>()
                .NodeConfig<Industry, DIndustry, VMIndustry, VMIndustryPageQuery, VMIndustryNode>()
                .NodeConfig<Location, DLocation, VMLocation, VMLocationPageQuery, VMLocationNode>()
                .ModelConfig<Talent, DTalent, VMTalent, VMTalentQuery>()
                .ModelConfig<Project, DProject, VMProject, VMProjectQuery>()
                .ModelConfig<Company, DCompany, VMCompany, VMCompanyQuery>()
                ;

            //! 添加安全相关
            services.AddSecurity();

            // !添加model映射
            services.AddAutoMapper(options => {
                options.AddProfile<DefaultProfile>();
            });

            services.AddScoped<IAccount, Account>();
            services.AddScoped<IEnterprise, Enterprise>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env) {
            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            }

            app.UseConfigurableCors();
            app.UseAuthentication();
            app.UseMvc();
        }
    }
}
