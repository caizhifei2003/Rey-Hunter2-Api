﻿using System.Collections.Generic;

namespace Rey.Hunter.Api {
    public class Constant {
        public static List<string> Permission => new List<string> {
            "talent.create", "talent.update", "talent.delete","talent.query"
        };
    }
}
