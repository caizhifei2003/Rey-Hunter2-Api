﻿using Rey.Mongo.Models.Data;
using Rey.Mongo.Models.View;

namespace Rey.Mongo {
    public interface INodeConfig<TDModel, TVMModel, TQuery, TNode> : IModelConfig<TDModel, TVMModel, TQuery>
        where TDModel : DNodeModel
        where TVMModel : VMNodeModel
        where TQuery : VMPageQuery
        where TNode : VMNode<TNode> {
    }
}
