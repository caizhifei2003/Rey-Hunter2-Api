﻿using MongoDB.Driver;
using Rey.Mongo.Builders;
using Rey.Mongo.Models.Data;
using Rey.Mongo.Models.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rey.Mongo {
    public class NodeService<TDModel, TVMModel, TQuery, TNode> : ModelService<TDModel, TVMModel, TQuery>, INodeService<TDModel, TVMModel, TQuery, TNode>
        where TDModel : DNodeModel
        where TVMModel : VMNodeModel
        where TQuery : VMPageQuery
        where TNode : VMNode<TNode> {

        public NodeService(IServiceProvider provider)
            : base(provider) {
        }

        public virtual async Task<IEnumerable<TNode>> QueryNodesAsync(TQuery query) {
            var filter = new AndFilterBuilder<TDModel>();

            this.GeneratorManager.GenerateQueryFilter(filter, query);

            var dModels = await this.Repo
                .Find(filter.Build())
                .ToListAsync();

            var nodes = this.Mapper.MapCollection<TNode>(dModels)
                .ToList();

            var roots = nodes.Where(x => x.ParentId == null);
            foreach (var root in roots) {
                AddChildren(nodes, root);
            }

            return roots;
        }

        protected void AddChildren(IEnumerable<TNode> nodes, TNode parent) {
            parent.Children = nodes.Where(x => x.ParentId == parent.Id).ToList();
            foreach (var child in parent.Children) {
                AddChildren(nodes, child);
            }
        }
    }
}
