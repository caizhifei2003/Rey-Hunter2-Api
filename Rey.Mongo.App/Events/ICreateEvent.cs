﻿using Rey.Mongo.Models.Data;
using System.Threading.Tasks;

namespace Rey.Mongo {
    public interface ICreateEvent {
        Task OnCreateAsync<TDModel>(TDModel dModel)
            where TDModel : DModel;
    }
}
