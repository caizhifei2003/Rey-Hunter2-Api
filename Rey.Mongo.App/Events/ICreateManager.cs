﻿using Rey.Mongo.Models.Data;
using System.Threading.Tasks;

namespace Rey.Mongo {
    public interface ICreateManager {
        Task OnCreateAsync<TDModel>(TDModel dModel)
            where TDModel : DModel;
    }
}
