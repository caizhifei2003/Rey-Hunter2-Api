﻿using Rey.Mongo.Models.Data;
using System.Threading.Tasks;

namespace Rey.Mongo {
    public interface IEventManager {
        Task OnCreateAsync<TDModel>(TDModel dModel) where TDModel : DModel;
        Task OnUpdateAsync<TDModel>(TDModel dModel) where TDModel : DModel;
        Task OnDeleteAsync<TDModel>(TDModel dModel) where TDModel : DModel;
    }
}
