﻿using Rey.Mongo.Models.Data;
using System.Threading.Tasks;

namespace Rey.Mongo {
    public interface IUpdateManager {
        Task OnUpdateAsync<TDModel>(TDModel dModel)
            where TDModel : DModel;
    }
}
