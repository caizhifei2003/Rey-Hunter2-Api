﻿using Rey.Mongo.Models.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Rey.Mongo {
    public class DeleteManager : IDeleteManager {
        protected IEnumerable<IDeleteEvent> Events { get; }

        public DeleteManager(IEnumerable<IDeleteEvent> events) {
            this.Events = events;
        }

        public async Task OnDeleteAsync<TDModel>(TDModel dModel)
            where TDModel : DModel {
            foreach (var e in this.Events) {
                await e.OnDeleteAsync(dModel);
            }
        }
    }
}
