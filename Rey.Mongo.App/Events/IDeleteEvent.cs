﻿using Rey.Mongo.Models.Data;
using System.Threading.Tasks;

namespace Rey.Mongo {
    public interface IDeleteEvent {
        Task OnDeleteAsync<TDModel>(TDModel dModel)
            where TDModel : DModel;
    }
}
