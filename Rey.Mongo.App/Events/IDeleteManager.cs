﻿using Rey.Mongo.Models.Data;
using System.Threading.Tasks;

namespace Rey.Mongo {
    public interface IDeleteManager {
        Task OnDeleteAsync<TDModel>(TDModel dModel)
            where TDModel : DModel;
    }
}
