﻿using Rey.Mongo.Models.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Rey.Mongo {
    public class CreateManager : ICreateManager {
        protected IEnumerable<ICreateEvent> Events { get; }

        public CreateManager(IEnumerable<ICreateEvent> events) {
            this.Events = events;
        }

        public async Task OnCreateAsync<TDModel>(TDModel dModel)
            where TDModel : DModel {
            foreach (var e in this.Events) {
                await e.OnCreateAsync(dModel);
            }
        }
    }
}
