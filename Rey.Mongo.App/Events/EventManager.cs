﻿using Rey.Mongo.Models.Data;
using System.Threading.Tasks;

namespace Rey.Mongo {
    public class EventManager : IEventManager {
        protected ICreateManager Create { get; }
        protected IUpdateManager Update { get; }
        protected IDeleteManager Delete { get; }

        public EventManager(
            ICreateManager create,
            IUpdateManager update,
            IDeleteManager delete
            ) {
            this.Create = create;
            this.Update = update;
            this.Delete = delete;
        }

        public async Task OnCreateAsync<TDModel>(TDModel dModel)
            where TDModel : DModel {
            await this.Create.OnCreateAsync(dModel);
        }

        public async Task OnDeleteAsync<TDModel>(TDModel dModel)
            where TDModel : DModel {
            await this.Delete.OnDeleteAsync(dModel);
        }

        public async Task OnUpdateAsync<TDModel>(TDModel dModel)
            where TDModel : DModel {
            await this.Update.OnUpdateAsync(dModel);
        }
    }
}
