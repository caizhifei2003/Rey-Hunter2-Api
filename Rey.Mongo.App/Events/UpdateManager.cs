﻿using Rey.Mongo.Models.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Rey.Mongo {
    public class UpdateManager : IUpdateManager {
        protected IEnumerable<IUpdateEvent> Events { get; }

        public UpdateManager(IEnumerable<IUpdateEvent> events) {
            this.Events = events;
        }

        public async Task OnUpdateAsync<TDModel>(TDModel dModel)
            where TDModel : DModel {
            foreach (var e in this.Events) {
                await e.OnUpdateAsync(dModel);
            }
        }
    }
}
