﻿using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;
using Rey.Mongo.Models;
using Rey.Mongo.Models.Data;
using Rey.Mongo.Models.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rey.Mongo.Helpers {
    public interface IAttachHelper<TDModel, TVMModel>
        where TDModel : DModel
        where TVMModel : VMModel {
        Task<TRefVMModel> RefAsync<TRefDModel, TRefVMModel>(RefModel<TRefDModel, TRefVMModel> refModel)
            where TRefDModel : DModel
            where TRefVMModel : VMModel;

        Task<IEnumerable<TRefVMModel>> RefAsync<TRefDModel, TRefVMModel>(IEnumerable<RefModel<TRefDModel, TRefVMModel>> refModels)
            where TRefDModel : DModel
            where TRefVMModel : VMModel;
    }

    public class AttachHelper<TDModel, TVMModel> : IAttachHelper<TDModel, TVMModel>
        where TDModel : DModel
        where TVMModel : VMModel {
        private IServiceProvider Provider { get; }
        private IModelMapper Mapper { get; }

        public AttachHelper(
            IModelMapper mapper,
            IServiceProvider provider
            ) {
            this.Mapper = mapper;
            this.Provider = provider;
        }

        public async Task<TRefVMModel> RefAsync<TRefDModel, TRefVMModel>(RefModel<TRefDModel, TRefVMModel> refModel)
            where TRefDModel : DModel
            where TRefVMModel : VMModel {
            if (refModel == null)
                return null;

            if (refModel.Id == null)
                throw new ArgumentNullException(nameof(refModel.Id));

            var repo = this.Provider.GetService<IRepository<TRefDModel>>();
            var dRefModel = await repo.Find(x => x.Id == refModel.Id).FirstOrDefaultAsync();
            if (dRefModel == null)
                return null;

            return this.Mapper.Map<TRefVMModel>(dRefModel);
        }

        public async Task<IEnumerable<TRefVMModel>> RefAsync<TRefDModel, TRefVMModel>(IEnumerable<RefModel<TRefDModel, TRefVMModel>> refModels)
            where TRefDModel : DModel
            where TRefVMModel : VMModel {
            if (refModels == null || refModels.Count() == 0)
                return new List<TRefVMModel>();

            var ids = refModels
                .Where(x => x != null)
                .Select(x => x.Id)
                .Where(x => x != null)
                .ToList();

            if (ids.Count == 0)
                return new List<TRefVMModel>();

            var filter = Builders<TRefDModel>.Filter.In(x => x.Id, ids);
            var repo = this.Provider.GetService<IRepository<TRefDModel>>();
            var dRefModels = await repo.Find(filter).ToListAsync();
            return this.Mapper.MapCollection<TRefVMModel>(dRefModels);
        }
    }
}
