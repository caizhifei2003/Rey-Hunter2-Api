﻿using Rey.Mongo.Models.Data;
using Rey.Mongo.Models.View;

namespace Rey.Mongo {
    public abstract class NodeConfig<TDModel, TVMModel, TQuery, TNode> : ModelConfig<TDModel, TVMModel, TQuery>, INodeConfig<TDModel, TVMModel, TQuery, TNode>
        where TDModel : DNodeModel
        where TVMModel : VMNodeModel
        where TQuery : VMPageQuery
        where TNode : VMNode<TNode> {
    }
}
