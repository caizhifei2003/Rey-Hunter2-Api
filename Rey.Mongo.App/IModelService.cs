﻿using Rey.Mongo.Models.Data;
using Rey.Mongo.Models.View;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Rey.Mongo {
    public interface IModelService<TDModel, TVMModel, TQuery>
        where TDModel : DModel
        where TVMModel : VMModel
        where TQuery : VMPageQuery {
        Task<TVMModel> CreateAsync(TVMModel model);
        Task<TVMModel> UpdateAsync(string id, TVMModel model);
        Task<TVMModel> DeleteAsync(string id);
        Task<IEnumerable<TVMModel>> BatchDeleteAsync(string[] ids);
        Task<TVMModel> GetAsync(string id);
        Task<IEnumerable<TVMModel>> QueryAsync(TQuery query);
        Task<VMPageData<TVMModel>> PageQueryAsync(TQuery query);
    }
}
