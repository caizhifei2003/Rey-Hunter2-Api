﻿using Rey.Mongo.Models.Data;
using Rey.Mongo.Models.View;

namespace Rey.Mongo {
    public interface IModelConfig<TDModel, TVMModel, TQuery> :
        ICreateEvent,
        IUpdateEvent,
        IDeleteEvent,
        IUpdateFilterGenerator<TDModel, TVMModel>,
        IUpdateGenerator<TDModel, TVMModel>,
        IDeleteFilterGenerator<TDModel>,
        IBatchDeleteFilterGenerator<TDModel>,
        IGetFilterGenerator<TDModel>,
        IQueryFilterGenerator<TDModel, TQuery>,
        IQuerySortGenerator<TDModel, TQuery>,
        IModelAttacher<TDModel, TVMModel>

        where TDModel : DModel
        where TVMModel : VMModel
        where TQuery : VMPageQuery {
    }
}
