﻿using System.Collections.Generic;

namespace Rey.Mongo {
    public interface IModelMapper {
        T Map<T>(object from);
        IEnumerable<T> MapCollection<T>(object from);
    }
}
