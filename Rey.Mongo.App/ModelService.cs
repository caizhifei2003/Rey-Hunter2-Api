﻿using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;
using Rey.Mongo.Builders;
using Rey.Mongo.Helpers;
using Rey.Mongo.Models.Data;
using Rey.Mongo.Models.View;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Rey.Mongo {
    public class ModelService<TDModel, TVMModel, TQuery> : IModelService<TDModel, TVMModel, TQuery>
        where TDModel : DModel
        where TVMModel : VMModel
        where TQuery : VMPageQuery {

        protected IModelMapper Mapper { get; }
        protected IRepository<TDModel> Repo { get; }
        protected IEventManager EventManager { get; }
        protected IGeneratorManager<TDModel, TVMModel, TQuery> GeneratorManager { get; }
        protected IModelAttacher<TDModel, TVMModel> Attacher { get; }
        protected IAttachHelper<TDModel, TVMModel> AttachHelper { get; }

        public ModelService(IServiceProvider provider) {
            this.Mapper = provider.GetService<IModelMapper>();
            this.Repo = provider.GetService<IRepository<TDModel>>();
            this.EventManager = provider.GetService<IEventManager>();
            this.GeneratorManager = provider.GetService<IGeneratorManager<TDModel, TVMModel, TQuery>>();
            this.Attacher = provider.GetService<IModelAttacher<TDModel, TVMModel>>();
            this.AttachHelper = provider.GetService<IAttachHelper<TDModel, TVMModel>>();
        }

        public async Task<TVMModel> CreateAsync(TVMModel model) {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            var dModel = this.Mapper.Map<TDModel>(model);
            await this.Repo.InsertOneAsync(dModel);
            await this.EventManager.OnCreateAsync(dModel);
            return this.Map(dModel);
        }

        public async Task<TVMModel> UpdateAsync(string id, TVMModel model) {
            if (id == null)
                throw new ArgumentNullException(nameof(id));

            if (model == null)
                throw new ArgumentNullException(nameof(model));

            var filter = new AndFilterBuilder<TDModel>();
            var update = new UpdateBuilder<TDModel>();

            this.GeneratorManager.GenerateUpdateFilter(filter, id, model);
            this.GeneratorManager.GenerateUpdate(update, id, model);

            var ret = await this.Repo.UpdateOneAsync(filter.Build(), update.Build());
            if (ret.MatchedCount == 0)
                return null;

            var dModel = await this.Repo.Find(filter.Build()).FirstOrDefaultAsync();
            await this.EventManager.OnUpdateAsync(dModel);
            return this.Map(dModel);
        }

        public async Task<TVMModel> DeleteAsync(string id) {
            if (id == null)
                throw new ArgumentNullException(nameof(id));

            var filter = new AndFilterBuilder<TDModel>();
            this.GeneratorManager.GenerateDeleteFilter(filter, id);

            var dModel = await this.Repo.FindOneAndDeleteAsync(filter.Build());
            if (dModel == null)
                return null;

            await this.EventManager.OnDeleteAsync(dModel);
            return this.Map(dModel);
        }

        public async Task<IEnumerable<TVMModel>> BatchDeleteAsync(string[] ids) {
            if (ids == null || ids.Length == 0)
                return new List<TVMModel>();

            var filter = new AndFilterBuilder<TDModel>();
            this.GeneratorManager.GenerateBatchDeleteFilter(filter, ids);

            var dModels = await this.Repo.Find(filter.Build()).ToListAsync();
            if (dModels.Count == 0)
                return new List<TVMModel>();

            await this.Repo.DeleteManyAsync(filter.Build());

            foreach (var dModel in dModels) {
                await this.EventManager.OnDeleteAsync(dModel);
            }

            return this.MapCollection(dModels);
        }

        public async Task<TVMModel> GetAsync(string id) {
            if (id == null)
                throw new ArgumentNullException(nameof(id));

            var filter = new AndFilterBuilder<TDModel>();
            this.GeneratorManager.GenerateGetFilter(filter, id);

            var dModel = await this.Repo.Find(filter.Build()).FirstOrDefaultAsync();
            if (dModel == null)
                return null;

            return this.Map(dModel);
        }

        public async Task<IEnumerable<TVMModel>> QueryAsync(TQuery query) {
            var filter = new AndFilterBuilder<TDModel>();
            var sort = new SortBuilder<TDModel>();

            this.GeneratorManager.GenerateQueryFilter(filter, query);
            this.GeneratorManager.GenerateQuerySort(sort, query);

            var dModels = await this.Repo
                .Find(filter.Build())
                .Sort(sort.Build())
                .ToListAsync();

            return this.MapCollection(dModels);
        }

        public async Task<VMPageData<TVMModel>> PageQueryAsync(TQuery query) {
            var filter = new AndFilterBuilder<TDModel>();
            var sort = new SortBuilder<TDModel>();

            this.GeneratorManager.GenerateQueryFilter(filter, query);
            this.GeneratorManager.GenerateQuerySort(sort, query);

            var fluent = this.Repo.Find(filter.Build());
            var total = await fluent.CountDocumentsAsync();

            fluent = fluent.Sort(sort.Build());

            if (query.Size > -1) {
                fluent = fluent
                    .Skip(Math.Max(0, (query.Page - 1) * query.Size))
                    .Limit(query.Size);
            }

            var dModels = await fluent.ToListAsync();
            return new VMPageData<TVMModel> {
                Items = this.MapCollection(dModels),
                Page = query.Page,
                Size = query.Size,
                Total = total,
            };
        }

        protected TVMModel Map(TDModel from) {
            var model = this.Mapper.Map<TVMModel>(from);
            return this.Attacher.AttachAsync(this.AttachHelper, from, model).GetAwaiter().GetResult();
        }

        protected IEnumerable<TVMModel> MapCollection(IEnumerable<TDModel> from) {
            var models = this.Mapper.MapCollection<TVMModel>(from);
            return this.Attacher.AttachAsync(this.AttachHelper, from, models).GetAwaiter().GetResult();
        }
    }
}
