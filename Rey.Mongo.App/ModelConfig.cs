﻿using Rey.Mongo.Builders;
using Rey.Mongo.Helpers;
using Rey.Mongo.Models.Data;
using Rey.Mongo.Models.View;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Rey.Mongo {
    public abstract class ModelConfig<TDModel, TVMModel, TQuery> : IModelConfig<TDModel, TVMModel, TQuery>
        where TDModel : DModel
        where TVMModel : VMModel
        where TQuery : VMPageQuery {
        public virtual void GenerateBatchDeleteFilter(IFilterBuilder<TDModel> builder, IEnumerable<string> ids) {
            builder.In(x => x.Id, ids);
        }

        public virtual void GenerateDeleteFilter(IFilterBuilder<TDModel> builder, string id) {
            builder.Eq(x => x.Id, id);
        }

        public virtual void GenerateGetFilter(IFilterBuilder<TDModel> builder, string id) {
            builder.Eq(x => x.Id, id);
        }

        public virtual void GenerateUpdateFilter(IFilterBuilder<TDModel> builder, string id, TVMModel model) {
            builder.Eq(x => x.Id, id);
        }

        public abstract void GenerateQueryFilter(IFilterBuilder<TDModel> builder, TQuery query);
        public abstract void GenerateQuerySort(ISortBuilder<TDModel> builder, TQuery query);
        public abstract void GenerateUpdate(IUpdateBuilder<TDModel> builder, string id, TVMModel model);

        public virtual Task OnCreateAsync<T>(T dModel) where T : DModel => Task.CompletedTask;
        public virtual Task OnDeleteAsync<T>(T dModel) where T : DModel => Task.CompletedTask;
        public virtual Task OnUpdateAsync<T>(T dModel) where T : DModel => Task.CompletedTask;

        public virtual Task<TVMModel> AttachAsync(IAttachHelper<TDModel, TVMModel> helper, TDModel from, TVMModel to) => Task.FromResult(to);
        public virtual Task<IEnumerable<TVMModel>> AttachAsync(IAttachHelper<TDModel, TVMModel> helper, IEnumerable<TDModel> from, IEnumerable<TVMModel> to) => Task.FromResult(to);
    }
}
