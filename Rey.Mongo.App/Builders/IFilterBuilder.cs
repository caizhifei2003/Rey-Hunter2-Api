﻿using MongoDB.Bson;
using MongoDB.Driver;
using Rey.Mongo.Models.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text.RegularExpressions;

namespace Rey.Mongo.Builders {
    public interface IFilterBuilder<TDModel>
        where TDModel : DModel {
        IFilterBuilder<TDModel> Filter(Func<FilterDefinitionBuilder<TDModel>, FilterDefinition<TDModel>> build);
        IFilterBuilder<TDModel> Eq<TField>(Expression<Func<TDModel, TField>> field, TField value);
        IFilterBuilder<TDModel> In<TField>(Expression<Func<TDModel, TField>> field, IEnumerable<TField> values);
        IFilterBuilder<TDModel> Like(Expression<Func<TDModel, object>> field, string value);
        IFilterBuilder<TDModel> OrLike(Expression<Func<TDModel, object>> field, IEnumerable<string> values);

        IFilterBuilder<TDModel> And(Action<IFilterBuilder<TDModel>> build);
        IFilterBuilder<TDModel> Or(Action<IFilterBuilder<TDModel>> build);
    }

    public abstract class FilterBuilder<TDModel> : IFilterBuilder<TDModel>
        where TDModel : DModel {
        protected List<FilterDefinition<TDModel>> _filters = new List<FilterDefinition<TDModel>>();

        public IFilterBuilder<TDModel> Filter(Func<FilterDefinitionBuilder<TDModel>, FilterDefinition<TDModel>> build) {
            return AddFilter(build(Builders<TDModel>.Filter));
        }

        public IFilterBuilder<TDModel> Eq<TField>(Expression<Func<TDModel, TField>> field, TField value) {
            return this.Filter(filter => filter.Eq(field, value));
        }

        public IFilterBuilder<TDModel> In<TField>(Expression<Func<TDModel, TField>> field, IEnumerable<TField> values) {
            return this.Filter(filter => filter.In(field, values));
        }

        public IFilterBuilder<TDModel> Like(Expression<Func<TDModel, object>> field, string value) {
            return this.Filter(filter => filter.Regex(field, new BsonRegularExpression(Regex.Escape(value), "i")));
        }

        public IFilterBuilder<TDModel> OrLike(Expression<Func<TDModel, object>> field, IEnumerable<string> values) {
            if (values == null || values.Count() == 0)
                return this;

            return this.Or(or => {
                foreach (var value in values) {
                    or.Like(field, value);
                }
            });
        }

        public IFilterBuilder<TDModel> And(Action<IFilterBuilder<TDModel>> build) {
            var builder = new AndFilterBuilder<TDModel>();
            build(builder);
            return this.AddFilter(builder.Build());
        }

        public IFilterBuilder<TDModel> Or(Action<IFilterBuilder<TDModel>> build) {
            var builder = new OrFilterBuilder<TDModel>();
            build(builder);
            return this.AddFilter(builder.Build());
        }

        public FilterDefinition<TDModel> Build() {
            if (this._filters.Count == 0)
                return Builders<TDModel>.Filter.Empty;

            if (this._filters.Count == 1)
                return this._filters.First();

            return this.Build(this._filters);
        }

        protected virtual IFilterBuilder<TDModel> AddFilter(FilterDefinition<TDModel> filter) {
            this._filters.Add(filter);
            return this;
        }

        protected abstract FilterDefinition<TDModel> Build(IEnumerable<FilterDefinition<TDModel>> filters);
    }

    public class AndFilterBuilder<TDModel> : FilterBuilder<TDModel>
        where TDModel : DModel {
        protected override FilterDefinition<TDModel> Build(IEnumerable<FilterDefinition<TDModel>> filters) {
            return Builders<TDModel>.Filter.And(filters);
        }
    }

    public class OrFilterBuilder<TDModel> : FilterBuilder<TDModel>
        where TDModel : DModel {
        protected override FilterDefinition<TDModel> Build(IEnumerable<FilterDefinition<TDModel>> filters) {
            return Builders<TDModel>.Filter.Or(filters);
        }
    }
}
