﻿using MongoDB.Driver;
using Rey.Mongo.Models.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Rey.Mongo.Builders {
    public interface ISortBuilder<TDModel>
        where TDModel : DModel {
        ISortBuilder<TDModel> Ascending(Expression<Func<TDModel, object>> field);
        ISortBuilder<TDModel> Descending(Expression<Func<TDModel, object>> field);
        ISortBuilder<TDModel> Sort(string name, string direction, Action<ISortItemBuilder<TDModel>> build);
    }

    public class SortItem<TDModel>
        where TDModel : DModel {
        public Expression<Func<TDModel, object>> Field { get; set; }
        public string Name { get; set; }

        public SortItem(Expression<Func<TDModel, object>> field, string name = null) {
            this.Field = field;
            this.Name = name ?? ((field.Body) as MemberExpression).Member.Name;
        }
    }

    public interface ISortItemBuilder<TDModel>
        where TDModel : DModel {
        ISortItemBuilder<TDModel> On(Expression<Func<TDModel, object>> field, string name = null);
    }

    public class SortItemBuilder<TDModel> : ISortItemBuilder<TDModel>
        where TDModel : DModel {
        protected List<SortItem<TDModel>> Items { get; } = new List<SortItem<TDModel>>();
        public ISortItemBuilder<TDModel> On(Expression<Func<TDModel, object>> field, string name = null) {
            this.Items.Add(new SortItem<TDModel>(field, name));
            return this;
        }

        public IEnumerable<SortItem<TDModel>> Build() {
            return this.Items;
        }
    }

    public class SortBuilder<TDModel> : ISortBuilder<TDModel>
        where TDModel : DModel {
        private readonly List<SortDefinition<TDModel>> _sorts = new List<SortDefinition<TDModel>>();

        public ISortBuilder<TDModel> Ascending(Expression<Func<TDModel, object>> field) {
            this._sorts.Add(Builders<TDModel>.Sort.Ascending(field));
            return this;
        }

        public ISortBuilder<TDModel> Descending(Expression<Func<TDModel, object>> field) {
            this._sorts.Add(Builders<TDModel>.Sort.Descending(field));
            return this;
        }

        public ISortBuilder<TDModel> Sort(string name, string direction, Action<ISortItemBuilder<TDModel>> build) {
            if (string.IsNullOrEmpty(name))
                return this;

            var builder = new SortItemBuilder<TDModel>();
            build.Invoke(builder);

            var items = builder.Build();
            var item = items.FirstOrDefault(x => x.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase));
            if (item == null)
                return this;

            var desc = direction == null || direction.Equals("desc", StringComparison.CurrentCultureIgnoreCase);
            var asc = direction.Equals("asc", StringComparison.CurrentCultureIgnoreCase);

            if (desc) {
                this.Descending(item.Field);
                return this;
            }

            if (asc) {
                this.Ascending(item.Field);
                return this;
            }

            throw new NotImplementedException();
        }

        public SortDefinition<TDModel> Build() {
            if (this._sorts.Count == 0)
                return Builders<TDModel>.Sort.Descending(x => x.CreateAt);

            if (this._sorts.Count == 1)
                return this._sorts.First();

            return Builders<TDModel>.Sort.Combine(this._sorts);
        }
    }
}
