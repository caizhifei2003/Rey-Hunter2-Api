﻿using MongoDB.Driver;
using Rey.Mongo.Models.Data;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Rey.Mongo.Builders {
    public interface IUpdateBuilder<TDModel>
        where TDModel : DModel {
        IUpdateBuilder<TDModel> Update(Func<UpdateDefinitionBuilder<TDModel>, UpdateDefinition<TDModel>> build);
        IUpdateBuilder<TDModel> Set<TField>(Expression<Func<TDModel, TField>> field, TField value);
    }

    public class UpdateBuilder<TDModel> : IUpdateBuilder<TDModel>
        where TDModel : DModel {

        private List<UpdateDefinition<TDModel>> _updates = new List<UpdateDefinition<TDModel>>();

        public IUpdateBuilder<TDModel> Update(Func<UpdateDefinitionBuilder<TDModel>, UpdateDefinition<TDModel>> build) {
            this._updates.Add(build(Builders<TDModel>.Update));
            return this;
        }

        public IUpdateBuilder<TDModel> Set<TField>(Expression<Func<TDModel, TField>> field, TField value) {
            return this.Update(builder => builder.Set(field, value));
        }

        public UpdateDefinition<TDModel> Build() {
            return Builders<TDModel>.Update.Combine(this._updates);
        }
    }
}
