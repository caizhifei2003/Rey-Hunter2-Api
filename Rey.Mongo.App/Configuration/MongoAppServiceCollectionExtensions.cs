﻿using Rey.Mongo;
using Rey.Mongo.Configuration;
using Rey.Mongo.Helpers;
using System;

namespace Microsoft.Extensions.DependencyInjection {
    public static class MongoAppServiceCollectionExtensions {
        public static IMongoAppBuilder AddMongoApp(this IServiceCollection services, Action<MongoAppOptions> configure = null) {
            var options = new MongoAppOptions();
            configure?.Invoke(options);
            services.AddSingleton(options);
            services.AddScoped(typeof(IModelService<,,>), typeof(ModelService<,,>));
            services.AddScoped(typeof(INodeService<,,,>), typeof(NodeService<,,,>));

            services.AddScoped(typeof(ICreateManager), typeof(CreateManager));
            services.AddScoped(typeof(IUpdateManager), typeof(UpdateManager));
            services.AddScoped(typeof(IDeleteManager), typeof(DeleteManager));
            services.AddScoped(typeof(IEventManager), typeof(EventManager));

            services.AddScoped(typeof(IUpdateFilterGenerator<,>), typeof(DefaultUpdateFilterGenerator<,>));
            services.AddScoped(typeof(IDeleteFilterGenerator<>), typeof(DefaultDeleteFilterGenerator<>));
            services.AddScoped(typeof(IBatchDeleteFilterGenerator<>), typeof(DefaultBatchDeleteFilterGenerator<>));
            services.AddScoped(typeof(IGetFilterGenerator<>), typeof(DefaultGetFilterGenerator<>));
            services.AddScoped(typeof(IGeneratorManager<,,>), typeof(GeneratorManager<,,>));

            services.AddScoped(typeof(IAttachHelper<,>), typeof(AttachHelper<,>));

            return new MongoAppBuilder(services);
        }
    }
}
