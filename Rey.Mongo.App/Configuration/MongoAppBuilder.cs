﻿using Microsoft.Extensions.DependencyInjection;
using Rey.Mongo.Models.Data;
using Rey.Mongo.Models.View;
using System;

namespace Rey.Mongo.Configuration {
    public class MongoAppBuilder : IMongoAppBuilder {
        private IServiceCollection Services { get; }

        public MongoAppBuilder(IServiceCollection services) {
            this.Services = services;
        }

        public IMongoAppBuilder Model<TDModel, TVMModel, TQuery>(Action<IMongoAppModelBuilder<TDModel, TVMModel, TQuery>> build)
            where TDModel : DModel
            where TVMModel : VMModel
            where TQuery : VMPageQuery {
            var builder = new MongoAppModelBuilder<TDModel, TVMModel, TQuery>(this.Services);
            build.Invoke(builder);
            return this;
        }

        public IMongoAppBuilder Mapper<TModelMapper>()
            where TModelMapper : class, IModelMapper {
            this.Services.AddScoped<IModelMapper, TModelMapper>();
            return this;
        }

        public IMongoAppBuilder ModelConfig<TModelConfig, TDModel, TVMModel, TQuery>()
            where TModelConfig : class, IModelConfig<TDModel, TVMModel, TQuery>
            where TDModel : DModel
            where TVMModel : VMModel
            where TQuery : VMPageQuery {
            return this.Model<TDModel, TVMModel, TQuery>(builder => builder
                .OnCreate<TModelConfig>()
                .OnUpdate<TModelConfig>()
                .OnDelete<TModelConfig>()
                .UpdateFilterGenerator<TModelConfig>()
                .UpdateGenerator<TModelConfig>()
                .DeleteFilterGenerator<TModelConfig>()
                .BatchDeleteFilterGenerator<TModelConfig>()
                .GetFilterGenerator<TModelConfig>()
                .QueryFilterGenerator<TModelConfig>()
                .QuerySortGenerator<TModelConfig>()
                .ModelAttacher<TModelConfig>()
            );
        }

        public IMongoAppBuilder NodeConfig<TNodeConfig, TDModel, TVMModel, TQuery, TNode>()
            where TNodeConfig : class, INodeConfig<TDModel, TVMModel, TQuery, TNode>
            where TDModel : DNodeModel
            where TVMModel : VMNodeModel
            where TQuery : VMPageQuery
            where TNode : VMNode<TNode> {
            return this.ModelConfig<TNodeConfig, TDModel, TVMModel, TQuery>();
        }
    }
}
