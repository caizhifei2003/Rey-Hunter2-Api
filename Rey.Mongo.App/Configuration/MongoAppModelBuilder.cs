﻿using Microsoft.Extensions.DependencyInjection;
using Rey.Mongo.Models.Data;
using Rey.Mongo.Models.View;

namespace Rey.Mongo.Configuration {
    public class MongoAppModelBuilder<TDModel, TVMModel, TQuery> : IMongoAppModelBuilder<TDModel, TVMModel, TQuery>
        where TDModel : DModel
        where TVMModel : VMModel
        where TQuery : VMPageQuery {
        private IServiceCollection Services { get; }

        public MongoAppModelBuilder(IServiceCollection services) {
            this.Services = services;
        }

        public IMongoAppModelBuilder<TDModel, TVMModel, TQuery> OnCreate<TCreateEvent>()
            where TCreateEvent : class, ICreateEvent {
            this.Services.AddScoped<ICreateEvent, TCreateEvent>();
            return this;
        }

        public IMongoAppModelBuilder<TDModel, TVMModel, TQuery> OnUpdate<TUpdateEvent>()
            where TUpdateEvent : class, IUpdateEvent {
            this.Services.AddScoped<IUpdateEvent, TUpdateEvent>();
            return this;
        }

        public IMongoAppModelBuilder<TDModel, TVMModel, TQuery> OnDelete<TDeleteEvent>()
            where TDeleteEvent : class, IDeleteEvent {
            this.Services.AddScoped<IDeleteEvent, TDeleteEvent>();
            return this;
        }

        public IMongoAppModelBuilder<TDModel, TVMModel, TQuery> UpdateFilterGenerator<TUpdateFilterGenerator>()
            where TUpdateFilterGenerator : class, IUpdateFilterGenerator<TDModel, TVMModel> {
            this.Services.AddScoped<IUpdateFilterGenerator<TDModel, TVMModel>, TUpdateFilterGenerator>();
            return this;
        }

        public IMongoAppModelBuilder<TDModel, TVMModel, TQuery> UpdateGenerator<TUpdateGenerator>()
            where TUpdateGenerator : class, IUpdateGenerator<TDModel, TVMModel> {
            this.Services.AddScoped<IUpdateGenerator<TDModel, TVMModel>, TUpdateGenerator>();
            return this;
        }

        public IMongoAppModelBuilder<TDModel, TVMModel, TQuery> DeleteFilterGenerator<TDeleteFilterGenerator>()
            where TDeleteFilterGenerator : class, IDeleteFilterGenerator<TDModel> {
            this.Services.AddScoped<IDeleteFilterGenerator<TDModel>, TDeleteFilterGenerator>();
            return this;
        }

        public IMongoAppModelBuilder<TDModel, TVMModel, TQuery> BatchDeleteFilterGenerator<TBatchDeleteFilterGenerator>()
            where TBatchDeleteFilterGenerator : class, IBatchDeleteFilterGenerator<TDModel> {
            this.Services.AddScoped<IBatchDeleteFilterGenerator<TDModel>, TBatchDeleteFilterGenerator>();
            return this;
        }

        public IMongoAppModelBuilder<TDModel, TVMModel, TQuery> GetFilterGenerator<TGetFilterGenerator>()
            where TGetFilterGenerator : class, IGetFilterGenerator<TDModel> {
            this.Services.AddScoped<IGetFilterGenerator<TDModel>, TGetFilterGenerator>();
            return this;
        }

        public IMongoAppModelBuilder<TDModel, TVMModel, TQuery> QueryFilterGenerator<TQueryFilterGenerator>()
            where TQueryFilterGenerator : class, IQueryFilterGenerator<TDModel, TQuery> {
            this.Services.AddScoped<IQueryFilterGenerator<TDModel, TQuery>, TQueryFilterGenerator>();
            return this;
        }

        public IMongoAppModelBuilder<TDModel, TVMModel, TQuery> QuerySortGenerator<TQuerySortGenerator>()
            where TQuerySortGenerator : class, IQuerySortGenerator<TDModel, TQuery> {
            this.Services.AddScoped<IQuerySortGenerator<TDModel, TQuery>, TQuerySortGenerator>();
            return this;
        }

        public IMongoAppModelBuilder<TDModel, TVMModel, TQuery> ModelAttacher<TModelAttacher>()
            where TModelAttacher : class, IModelAttacher<TDModel, TVMModel> {
            this.Services.AddScoped<IModelAttacher<TDModel, TVMModel>, TModelAttacher>();
            return this;
        }
    }
}
