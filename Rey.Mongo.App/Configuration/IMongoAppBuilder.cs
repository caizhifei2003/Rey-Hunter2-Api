﻿using Rey.Mongo.Models.Data;
using Rey.Mongo.Models.View;
using System;

namespace Rey.Mongo.Configuration {
    public interface IMongoAppBuilder {
        IMongoAppBuilder Model<TDModel, TVMModel, TQuery>(Action<IMongoAppModelBuilder<TDModel, TVMModel, TQuery>> build)
            where TDModel : DModel
            where TVMModel : VMModel
            where TQuery : VMPageQuery;

        IMongoAppBuilder Mapper<TModelMapper>()
            where TModelMapper : class, IModelMapper;

        IMongoAppBuilder ModelConfig<TModelConfig, TDModel, TVMModel, TQuery>()
            where TModelConfig : class, IModelConfig<TDModel, TVMModel, TQuery>
            where TDModel : DModel
            where TVMModel : VMModel
            where TQuery : VMPageQuery;

        IMongoAppBuilder NodeConfig<TNodeConfig, TDModel, TVMModel, TQuery, TNode>()
            where TNodeConfig : class, INodeConfig<TDModel, TVMModel, TQuery, TNode>
            where TDModel : DNodeModel
            where TVMModel : VMNodeModel
            where TQuery : VMPageQuery
            where TNode : VMNode<TNode>;
    }
}
