﻿using Rey.Mongo.Models.Data;
using Rey.Mongo.Models.View;

namespace Rey.Mongo.Configuration {
    public interface IMongoAppModelBuilder<TDModel, TVMModel, TQuery>
        where TDModel : DModel
        where TVMModel : VMModel
        where TQuery : VMPageQuery {

        IMongoAppModelBuilder<TDModel, TVMModel, TQuery> OnCreate<TCreateEvent>()
            where TCreateEvent : class, ICreateEvent;

        IMongoAppModelBuilder<TDModel, TVMModel, TQuery> OnUpdate<TUpdateEvent>()
            where TUpdateEvent : class, IUpdateEvent;

        IMongoAppModelBuilder<TDModel, TVMModel, TQuery> OnDelete<TDeleteEvent>()
            where TDeleteEvent : class, IDeleteEvent;

        IMongoAppModelBuilder<TDModel, TVMModel, TQuery> UpdateFilterGenerator<TUpdateFilterGenerator>()
            where TUpdateFilterGenerator : class, IUpdateFilterGenerator<TDModel, TVMModel>;

        IMongoAppModelBuilder<TDModel, TVMModel, TQuery> UpdateGenerator<TUpdateGenerator>()
            where TUpdateGenerator : class, IUpdateGenerator<TDModel, TVMModel>;

        IMongoAppModelBuilder<TDModel, TVMModel, TQuery> DeleteFilterGenerator<TDeleteFilterGenerator>()
            where TDeleteFilterGenerator : class, IDeleteFilterGenerator<TDModel>;

        IMongoAppModelBuilder<TDModel, TVMModel, TQuery> BatchDeleteFilterGenerator<TBatchDeleteFilterGenerator>()
            where TBatchDeleteFilterGenerator : class, IBatchDeleteFilterGenerator<TDModel>;

        IMongoAppModelBuilder<TDModel, TVMModel, TQuery> GetFilterGenerator<TGetFilterGenerator>()
            where TGetFilterGenerator : class, IGetFilterGenerator<TDModel>;

        IMongoAppModelBuilder<TDModel, TVMModel, TQuery> QueryFilterGenerator<TQueryFilterGenerator>()
            where TQueryFilterGenerator : class, IQueryFilterGenerator<TDModel, TQuery>;

        IMongoAppModelBuilder<TDModel, TVMModel, TQuery> QuerySortGenerator<TQuerySortGenerator>()
            where TQuerySortGenerator : class, IQuerySortGenerator<TDModel, TQuery>;

        IMongoAppModelBuilder<TDModel, TVMModel, TQuery> ModelAttacher<TModelAttacher>()
            where TModelAttacher : class, IModelAttacher<TDModel, TVMModel>;
    }
}
