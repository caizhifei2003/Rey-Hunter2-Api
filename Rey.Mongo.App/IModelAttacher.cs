﻿using Rey.Mongo.Helpers;
using Rey.Mongo.Models.Data;
using Rey.Mongo.Models.View;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Rey.Mongo {
    public interface IModelAttacher<TDModel, TVMModel>
        where TDModel : DModel
        where TVMModel : VMModel {
        Task<TVMModel> AttachAsync(IAttachHelper<TDModel, TVMModel> helper, TDModel from, TVMModel to);
        Task<IEnumerable<TVMModel>> AttachAsync(IAttachHelper<TDModel, TVMModel> helper, IEnumerable<TDModel> from, IEnumerable<TVMModel> to);
    }
}
