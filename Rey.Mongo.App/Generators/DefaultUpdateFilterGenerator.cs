﻿using Rey.Mongo.Builders;
using Rey.Mongo.Models.Data;
using Rey.Mongo.Models.View;

namespace Rey.Mongo {
    public class DefaultUpdateFilterGenerator<TDModel, TVMModel> : IUpdateFilterGenerator<TDModel, TVMModel>
        where TDModel : DModel
        where TVMModel : VMModel {
        public void GenerateUpdateFilter(IFilterBuilder<TDModel> builder, string id, TVMModel model) {
            builder.Eq(x => x.Id, id);
        }
    }
}
