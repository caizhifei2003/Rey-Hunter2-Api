﻿using Rey.Mongo.Builders;
using Rey.Mongo.Models.Data;

namespace Rey.Mongo {
    public class DefaultDeleteFilterGenerator<TDModel> : IDeleteFilterGenerator<TDModel>
        where TDModel : DModel {
        public void GenerateDeleteFilter(IFilterBuilder<TDModel> builder, string id) {
            builder.Eq(x => x.Id, id);
        }
    }
}
