﻿using Rey.Mongo.Builders;
using Rey.Mongo.Models.Data;
using Rey.Mongo.Models.View;
using System.Collections.Generic;

namespace Rey.Mongo {
    public interface IGeneratorManager<TDModel, TVMModel, TQuery>
        where TDModel : DModel
        where TVMModel : VMModel
        where TQuery : VMPageQuery {
        void GenerateUpdateFilter(IFilterBuilder<TDModel> builder, string id, TVMModel model);
        void GenerateUpdate(IUpdateBuilder<TDModel> builder, string id, TVMModel model);
        void GenerateDeleteFilter(IFilterBuilder<TDModel> builder, string id);
        void GenerateBatchDeleteFilter(IFilterBuilder<TDModel> builder, IEnumerable<string> ids);
        void GenerateGetFilter(IFilterBuilder<TDModel> builder, string id);
        void GenerateQueryFilter(IFilterBuilder<TDModel> builder, TQuery query);
        void GenerateQuerySort(ISortBuilder<TDModel> builder, TQuery query);
    }
}
