﻿using Rey.Mongo.Builders;
using Rey.Mongo.Models.Data;

namespace Rey.Mongo {
    public interface IGetFilterGenerator<TDModel>
        where TDModel : DModel {
        void GenerateGetFilter(IFilterBuilder<TDModel> builder, string id);
    }
}
