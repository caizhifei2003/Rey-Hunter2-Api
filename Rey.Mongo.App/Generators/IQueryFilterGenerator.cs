﻿using Rey.Mongo.Builders;
using Rey.Mongo.Models.Data;
using Rey.Mongo.Models.View;

namespace Rey.Mongo {
    public interface IQueryFilterGenerator<TDModel, TQuery>
        where TDModel : DModel
        where TQuery : VMQuery {
        void GenerateQueryFilter(IFilterBuilder<TDModel> builder, TQuery query);
    }
}
