﻿using Rey.Mongo.Builders;
using Rey.Mongo.Models.Data;
using Rey.Mongo.Models.View;

namespace Rey.Mongo {
    public interface IUpdateFilterGenerator<TDModel, TVMModel>
        where TDModel : DModel
        where TVMModel : VMModel {
        void GenerateUpdateFilter(IFilterBuilder<TDModel> builder, string id, TVMModel model);
    }
}
