﻿using Rey.Mongo.Builders;
using Rey.Mongo.Models.Data;
using System.Collections.Generic;
using System.Linq;

namespace Rey.Mongo {
    public class DefaultBatchDeleteFilterGenerator<TDModel> : IBatchDeleteFilterGenerator<TDModel>
        where TDModel : DModel {
        public void GenerateBatchDeleteFilter(IFilterBuilder<TDModel> builder, IEnumerable<string> ids) {
            builder.In(x => x.Id, ids.ToList());
        }
    }
}
