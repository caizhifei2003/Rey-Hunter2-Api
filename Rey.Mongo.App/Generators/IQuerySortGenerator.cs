﻿using Rey.Mongo.Builders;
using Rey.Mongo.Models.Data;
using Rey.Mongo.Models.View;

namespace Rey.Mongo {
    public interface IQuerySortGenerator<TDModel, TQuery>
        where TDModel : DModel
        where TQuery : VMQuery {
        void GenerateQuerySort(ISortBuilder<TDModel> builder, TQuery query);
    }
}
