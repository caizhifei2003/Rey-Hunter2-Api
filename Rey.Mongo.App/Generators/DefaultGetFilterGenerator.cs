﻿using Rey.Mongo.Builders;
using Rey.Mongo.Models.Data;

namespace Rey.Mongo {
    public class DefaultGetFilterGenerator<TDModel> : IGetFilterGenerator<TDModel>
        where TDModel : DModel {
        public void GenerateGetFilter(IFilterBuilder<TDModel> builder, string id) {
            builder.Eq(x => x.Id, id);
        }
    }
}
