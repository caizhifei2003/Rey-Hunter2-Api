﻿using Rey.Mongo.Builders;
using Rey.Mongo.Models.Data;
using Rey.Mongo.Models.View;

namespace Rey.Mongo {
    public interface IUpdateGenerator<TDModel, TVMModel>
        where TDModel : DModel
        where TVMModel : VMModel {
        void GenerateUpdate(IUpdateBuilder<TDModel> builder, string id, TVMModel model);
    }
}
