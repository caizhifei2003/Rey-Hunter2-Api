﻿using Rey.Mongo.Builders;
using Rey.Mongo.Models.Data;
using System.Collections.Generic;

namespace Rey.Mongo {
    public interface IBatchDeleteFilterGenerator<TDModel>
        where TDModel : DModel {
        void GenerateBatchDeleteFilter(IFilterBuilder<TDModel> builder, IEnumerable<string> ids);
    }
}
