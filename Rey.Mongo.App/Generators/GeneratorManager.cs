﻿using Rey.Mongo.Builders;
using Rey.Mongo.Models.Data;
using Rey.Mongo.Models.View;
using System.Collections.Generic;

namespace Rey.Mongo {
    public class GeneratorManager<TDModel, TVMModel, TQuery> : IGeneratorManager<TDModel, TVMModel, TQuery>
        where TDModel : DModel
        where TVMModel : VMModel
        where TQuery : VMPageQuery {

        protected IUpdateFilterGenerator<TDModel, TVMModel> UpdateFilterGenerator { get; }
        protected IUpdateGenerator<TDModel, TVMModel> UpdateGenerator { get; }
        protected IDeleteFilterGenerator<TDModel> DeleteFilterGenerator { get; }
        protected IBatchDeleteFilterGenerator<TDModel> BatchDeleteFilterGenerator { get; }
        protected IGetFilterGenerator<TDModel> GetFilterGenerator { get; }
        protected IQueryFilterGenerator<TDModel, TQuery> QueryFilterGenerator { get; }
        protected IQuerySortGenerator<TDModel, TQuery> QuerySortGenerator { get; }

        public GeneratorManager(
            IUpdateFilterGenerator<TDModel, TVMModel> updateFilterGenerator = null,
            IUpdateGenerator<TDModel, TVMModel> updateGenerator = null,
            IDeleteFilterGenerator<TDModel> deleteFilterGenerator = null,
            IBatchDeleteFilterGenerator<TDModel> batchDeleteFilterGenerator = null,
            IGetFilterGenerator<TDModel> getFilterGenerator = null,
            IQueryFilterGenerator<TDModel, TQuery> queryFilterGenerator = null,
            IQuerySortGenerator<TDModel, TQuery> querySortGenerator = null
            ) {
            this.UpdateFilterGenerator = updateFilterGenerator;
            this.UpdateGenerator = updateGenerator;
            this.DeleteFilterGenerator = deleteFilterGenerator;
            this.BatchDeleteFilterGenerator = batchDeleteFilterGenerator;
            this.GetFilterGenerator = getFilterGenerator;
            this.QueryFilterGenerator = queryFilterGenerator;
            this.QuerySortGenerator = querySortGenerator;
        }

        public void GenerateUpdateFilter(IFilterBuilder<TDModel> builder, string id, TVMModel model) {
            this.UpdateFilterGenerator.GenerateUpdateFilter(builder, id, model);
        }

        public void GenerateUpdate(IUpdateBuilder<TDModel> builder, string id, TVMModel model) {
            this.UpdateGenerator.GenerateUpdate(builder, id, model);
        }

        public void GenerateDeleteFilter(IFilterBuilder<TDModel> builder, string id) {
            this.DeleteFilterGenerator.GenerateDeleteFilter(builder, id);
        }

        public void GenerateBatchDeleteFilter(IFilterBuilder<TDModel> builder, IEnumerable<string> ids) {
            this.BatchDeleteFilterGenerator.GenerateBatchDeleteFilter(builder, ids);
        }

        public void GenerateGetFilter(IFilterBuilder<TDModel> builder, string id) {
            this.GetFilterGenerator.GenerateGetFilter(builder, id);
        }

        public void GenerateQueryFilter(IFilterBuilder<TDModel> builder, TQuery query) {
            this.QueryFilterGenerator.GenerateQueryFilter(builder, query);
        }

        public void GenerateQuerySort(ISortBuilder<TDModel> builder, TQuery query) {
            this.QuerySortGenerator.GenerateQuerySort(builder, query);
        }
    }
}
