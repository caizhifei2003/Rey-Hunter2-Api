﻿using Rey.Mongo.Builders;
using Rey.Mongo.Models.Data;

namespace Rey.Mongo {
    public interface IDeleteFilterGenerator<TDModel>
        where TDModel : DModel {
        void GenerateDeleteFilter(IFilterBuilder<TDModel> builder, string id);
    }
}
