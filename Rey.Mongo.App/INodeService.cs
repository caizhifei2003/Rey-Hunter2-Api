﻿using Rey.Mongo.Models.Data;
using Rey.Mongo.Models.View;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Rey.Mongo {
    public interface INodeService<TDModel, TVMModel, TQuery, TNode> : IModelService<TDModel, TVMModel, TQuery>
        where TDModel : DNodeModel
        where TVMModel : VMNodeModel
        where TQuery : VMPageQuery
        where TNode : VMNode<TNode> {
        Task<IEnumerable<TNode>> QueryNodesAsync(TQuery query);
    }
}
