﻿using Microsoft.AspNetCore.Mvc;
using Rey.Mvc;
using Rey.Mvc.Filters;
using System;

namespace Microsoft.Extensions.DependencyInjection {
    public static class MvcOptionsExtensions {
        public static MvcOptions ReyApi(this MvcOptions mvcOptions, Action<ReyMvcOptions> configure = null) {
            var options = new ReyMvcOptions();
            configure?.Invoke(options);

            if (options.CatchAllException)
                mvcOptions.Filters.Add<ApiAllExceptionFilter>();
            else
                mvcOptions.Filters.Add<ApiExceptionFilter>();

            mvcOptions.Filters.Add<ApiResultFilter>();
            return mvcOptions;
        }
    }
}
