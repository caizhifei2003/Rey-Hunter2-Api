﻿namespace Rey.Mvc {
    public class ReyMvcOptions {
        public bool CatchAllException { get; set; } = true;
    }
}
