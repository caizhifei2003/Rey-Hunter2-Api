﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;

namespace Rey.Mvc.Filters {
    public class ApiAllExceptionFilter : IExceptionFilter {
        private readonly IHostingEnvironment _env;
        private readonly ILogger<ApiExceptionFilter> _logger;

        public ApiAllExceptionFilter(IHostingEnvironment env, ILogger<ApiExceptionFilter> logger) {
            this._env = env;
            this._logger = logger;
        }

        public void OnException(ExceptionContext context) {
            var exception = context.Exception;
            this._logger.LogWarning(exception, exception.Message);

            var resp = new {
                data = new { },
                //error = this._env.IsDevelopment() ? new DebugApiError(exception) : new ApiError(exception),
                error = new DebugApiError(exception),
            };

            context.Result = new JsonResult(resp);
        }
    }
}
