﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Rey.Mvc.Filters {
    public class ApiResultFilter : IResultFilter {
        public void OnResultExecuted(ResultExecutedContext context) {
        }

        public void OnResultExecuting(ResultExecutingContext context) {
            var result = context.Result;
            if (result is EmptyResult) {
                context.Result = ToJsonResult();
                return;
            }

            if (result is ObjectResult) {
                context.Result = ToJsonResult((result as ObjectResult)?.Value);
                return;
            }
        }

        private static IActionResult ToJsonResult(object data = null) {
            var code = ApiExceptionCode.SUCCEEDED;
            var resp = new {
                data = data ?? new { },
                error = new {
                    code,
                    message = ApiException.GetCodeMessage(code)
                }
            };
            return new JsonResult(resp);
        }
    }
}
